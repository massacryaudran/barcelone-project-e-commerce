<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // ? Pot Seeders

        Product::create([
    
            'name'=>'Small pot',
            'category_id'=>'2',
            'slug'=>'small_pot',
            'subtitle'=>'PET pot for indoor plants',
            'description'=>'With its small size, this pot will be perfect for decorating your interiors. Whether on a table or shelf, it can hold most flowers and even some climbing plants without restricting or damaging them',
            'characteristic_color'=>'Depend',
            'characteristic_dimension'=>'19 x 19 x 6.5 cm',
            'characteristic_weight'=>'160 g',
            'characteristic_materials'=>'PET',
            'price'=>'1000',
            'image'=>'products\November2022\D2ZlXPhBpDiAPhwsg8h1.PNG',          
        ]); 

        Product::create([
    
            'name'=>'Gardener',
            'slug'=>'gardener',
            'category_id'=>'2',
            'subtitle'=>'PET gardener for window sill',
            'description'=>'With a longer length than our other two products, the planter has been designed for window sills. It can contain up to 4 different plants for more variety. It is perfect for flowers or small fruits or vegetables (tomatoes, strawberries...).',
            'characteristic_color'=>'Depend',
            'characteristic_dimension'=>'49 x 19.5 x 20 cm',
            'characteristic_weight'=>'500 g',
            'characteristic_materials'=>'PET',
            'price'=>'2000',
            'image'=>'products\November2022\QB1AK2e2fGGuVOWPqnDe.PNG',          
        ]); 

        Product::create([
    
            'name'=>'Large pot',
            'slug'=>'large_pot',
            'category_id'=>'2',
            'subtitle'=>'PET pot for indoor & outdoor plants',
            'description'=>'With its large size, this pot perfectly serves indoor and outdoor plants. It can be held on a balcony, a terrace, in the open air or indoors. With these features, it can also contain an assortment of flowers or a small tree. Its neutral and lightweight design allows it to highlight its content despite its size.',
            'characteristic_color'=>'Depend',
            'characteristic_dimension'=>'30 x 30 x 30 cm',
            'characteristic_weight'=>'500 g',
            'characteristic_materials'=>'PET',
            'price'=>'2500',
            'image'=>'products\November2022\nNjEgsQSezc3o14rHtsJ.PNG',          
        ]); 

        // ? Tools Seeds

        Product::create([
    
            'name'=>'Shovel',
            'category_id'=>'1',
            'slug'=>'shovel',
            'subtitle'=>'Steel and PET shovel for plant maintenance ',
            'description'=>'With its sharp blade made of recycled steel, it will be a perfect alié for all your maintenance work. Easy to hold in your hand, you can easily handle dirt. This tool is designed to accompany the gesture of the user without hurting him with a safety clasp.',
            'characteristic_color'=>'Black',
            'characteristic_dimension'=>'45.7 x 4.6 x 15.2 cm',
            'characteristic_weight'=>'160 g',
            'characteristic_materials'=>'Steel and PET',
            'price'=>'2150',
            'image'=>'products\November2022\3t14ceTFqjGgkZUvAl9f.PNG',          
        ]); 

        Product::create([
    
            'name'=>'Watering can',
            'slug'=>'watering_can',
            'category_id'=>'1',
            'subtitle'=>'PET watering can for watering your indoor and outdoor plants',
            'description'=>'The watering can will meet all your needs and those of your plants. Made of PET plastic, it is easy and pleasant to use and can hold up to 11 liters of water. With its handle, it guarantees less effort once filled.',
            'characteristic_color'=>'Green',
            'characteristic_dimension'=>'17.5 x 63.5 x 42 cm',
            'characteristic_weight'=>'600 g',
            'characteristic_materials'=>'PET',
            'price'=>'1200',
            'image'=>'products\November2022\eryGBblUs7XKR09fhXku.PNG',          
        ]); 

        Product::create([
    
            'name'=>'Transplanter',
            'slug'=>'transplanter',
             'category_id'=>'1',
            'subtitle'=>'Steel, wood and PET transplanter for manual work',
            'description'=>'Thanks to its small size, this tool can be transported everywhere with ease. With its steel blade, you can plant all your seeds easily without getting your hands dirty. Its wooden and plastic handle will make it easier to fit your hands for long-term work without exhaustion or pain.',
            'characteristic_color'=>'Purple (plastic part)',
            'characteristic_dimension'=>'29.5 x 8 x 4 cm',
            'characteristic_weight'=>'220 g',
            'characteristic_materials'=>'Steel, wood and PET',
            'price'=>'1350',
            'image'=>'products\November2022\Q0m4guVWltWSQdZXsS77.PNG',          
        ]); 

        Product::create([
    
            'name'=>'Hand rake',
            'slug'=>'hand_rake',
             'category_id'=>'1',
            'subtitle'=>'Hand rake in steel, wood and PET for all your manual work',
            'description'=>'Easy to use, this hand rake will allow quick maintenance of your green spaces. With its small size, it will be easy to pass in all small places to remove dead leaves, petals or other elements disturbing the plants in their growth.',
            'characteristic_color'=>'Purple (plastic part)',
            'characteristic_dimension'=>'33.5 x 8.8 x 7.5 cm',
            'characteristic_weight'=>'225 g',
            'characteristic_materials'=>'Steel, wood and PET',
            'price'=>'1600',
            'image'=>'products\November2022\SQVLA9y6ENAr2JykTt3X.PNG',          
        ]); 

        // ? Books Seeders

        Product::create([
    
            'name'=>'Seasonal book (Summer)',
            'category_id'=>'3',
            'slug'=>'summer_book',
            'subtitle'=>'Summer book in recycled paper',
            'description'=>'Whether you are a beginner or a seasoned gardener, this book contains all the essential information for many seasonal plants. With him, finish the long research on the habits or needs of a plant. With its 300 pages, more than 250 common species (or not) are referenced.',
            'characteristic_color'=>'Yellow',
            'characteristic_dimension'=>'15.5 x 2.5 x 22.5 cm',
            'characteristic_weight'=>'510 g',
            'characteristic_materials'=>'Recycled paper',
            'price'=>'2000',
            'image'=>'products\November2022\mVPCKJ2tGzJtptnNn53i.PNG',          
        ]); 

        Product::create([
    
            'name'=>'Seasonal book (Spring)',
            'category_id'=>'3',
            'slug'=>'spring_book',
            'subtitle'=>'Spring book in recycled paper',
            'description'=>'Whether you are a beginner or a seasoned gardener, this book contains all the essential information for many seasonal plants. With him, finish the long research on the habits or needs of a plant. With its 300 pages, more than 250 common species (or not) are referenced.',
            'characteristic_color'=>'Dark green',
            'characteristic_dimension'=>'15.5 x 2.5 x 22.5 cm',
            'characteristic_weight'=>'510 g',
            'characteristic_materials'=>'Recycled paper',
            'price'=>'2000',
            'image'=>'products\November2022\sFp4dqvGaJpeHiTjUv4Q.PNG',          
        ]);  

        Product::create([
    
            'name'=>'Seasonal book (Winter)',
            'category_id'=>'3',
            'slug'=>'winter_book',
            'subtitle'=>'Winter book in recycled paper',
            'description'=>'Whether you are a beginner or a seasoned gardener, this book contains all the essential information for many seasonal plants. With him, finish the long research on the habits or needs of a plant. With its 300 pages, more than 250 common species (or not) are referenced.',
            'characteristic_color'=>'Light blue',
            'characteristic_dimension'=>'15.5 x 2.5 x 22.5 cm',
            'characteristic_weight'=>'510 g',
            'characteristic_materials'=>'Recycled paper',
            'price'=>'2000',
            'image'=>'products\November2022\FagCjlKDF8kPtq4KCmiM.PNG',          
        ]);   

        Product::create([
    
            'name'=>'Seasonal book (Fall)',
            'category_id'=>'3',
            'slug'=>'fall_book',
            'subtitle'=>'Fall book in recycled paper',
            'description'=>'Whether you are a beginner or a seasoned gardener, this book contains all the essential information for many seasonal plants. With him, finish the long research on the habits or needs of a plant. With its 300 pages, more than 250 common species (or not) are referenced.',
            'characteristic_color'=>'Orange',
            'characteristic_dimension'=>'15.5 x 2.5 x 22.5 cm',
            'characteristic_weight'=>'510 g',
            'characteristic_materials'=>'Recycled paper',
            'price'=>'2000',
            'image'=>'products\November2022\yNaZx8VnbW3LbT3jGVUB.PNG',          
        ]); 

        // ? Seeds Seeders

        Product::create([
            
            'name'=>'Seasonal seeds (Winter)',
            'category_id'=>'5',
            'slug'=>'winter_seeds',
            'subtitle'=>'Assortment of Winter (flowers and vegetables)',
            'description'=>'If you want to try gardening, this assortment is what you need. This pack contains random seeds with an equal proportion of flowers and vegetables/fruits of season. With this one, you are free to plant what you prefer and impress your guests',
            'price'=>'2500',
            'characteristic_color'=>'Depends of the flower',
            'characteristic_dimension'=>'none',
            'characteristic_weight'=>'20 g',
            'characteristic_materials'=>'Seeds',
            'image'=>'products\November2022\8VwCVGgFcYiSLmG8eVf6.PNG',          
        ]);

        Product::create([
            
            'name'=>'Seasonal seeds (Summer)',
            'category_id'=>'5',
            'slug'=>'summer_seeds',
            'subtitle'=>'Assortment of Summer (flowers and vegetables)',
            'description'=>'If you want to try gardening, this assortment is what you need. This pack contains random seeds with an equal proportion of flowers and vegetables/fruits of season. With this one, you are free to plant what you prefer and impress your guests',
            'price'=>'2500',
            'characteristic_color'=>'Depends of the flower',
            'characteristic_dimension'=>'none',
            'characteristic_weight'=>'20 g',
            'characteristic_materials'=>'Seeds',
            'image'=>'products\November2022\P1QYLm7n2IJxZcVYeYlf.PNG',          
        ]);

        Product::create([
            
            'name'=>'Seasonal seeds (Fall)',
            'category_id'=>'5',
            'slug'=>'fall_seeds',
            'subtitle'=>'Assortment of Fall (flowers and vegetables)',
            'description'=>'If you want to try gardening, this assortment is what you need. This pack contains random seeds with an equal proportion of flowers and vegetables/fruits of season. With this one, you are free to plant what you prefer and impress your guests',
            'price'=>'2500',
            'characteristic_color'=>'Depends of the flower',
            'characteristic_dimension'=>'none',
            'characteristic_weight'=>'20 g',
            'characteristic_materials'=>'Seeds',
            'image'=>'products\November2022\IFXHVifO3r3XeHgiFLGl.PNG',          
        ]);

        Product::create([
            
            'name'=>'Seasonal seeds (Spring)',
            'category_id'=>'5',
            'slug'=>'spring_seeds',
            'subtitle'=>'Assortment of Spring (flowers and vegetables)',
            'description'=>'If you want to try gardening, this assortment is what you need. This pack contains random seeds with an equal proportion of flowers and vegetables/fruits of season. With this one, you are free to plant what you prefer and impress your guests',
            'price'=>'2500',
            'characteristic_color'=>'Depends of the flower',
            'characteristic_dimension'=>'none',
            'characteristic_weight'=>'20 g',
            'characteristic_materials'=>'Seeds',
            'image'=>'products\November2022\rB8qZlIRGArbCTzXwsm1.PNG',          
        ]);

        // ? Fertilizers seeders

        Product::create([
            
            'name'=>'Seaweed fertilizer',
            'category_id'=>'4',
            'slug'=>'seaweed_fertilizer',
            'subtitle'=>'Seaweed fertilizer for your plants',
            'description'=>'For your plants, nothing is better than a 100% natural soil. Made from seaweed, this one will guarantee you a quick and trouble-free thumb of plants. In the form of mini debris, a sweet iodized smell will emerge. The product contains 10L of fertilizer.',
            'price'=>'1250',
            'characteristic_color'=>'Dark green and brown',
            'characteristic_dimension'=>'20 x 20 x 20 cm',
            'characteristic_weight'=>'4.2 kg',
            'characteristic_materials'=>'Seaweed',
            'image'=>'products\November2022\gP6NLN3qPjqBjCLuA4pd.PNG',          
        ]);

        Product::create([
            
            'name'=>'Seagull waste fertilizer',
            'category_id'=>'4',
            'slug'=>'seagull_fertilizer',
            'subtitle'=>'Seagull waste fertilizer for your plants',
            'description'=>'For your plants, nothing is better than a 100% natural soil. Made from seagull waste, this one will guarantee you a quick and trouble-free thumb of plants. In the form of mini debris, a sweet iodized smell will emerge. The product contains 10L of fertilizer',
            'price'=>'1250',
            'characteristic_color'=>'Gray and brown',
            'characteristic_dimension'=>'20 x 20 x 20 cm',
            'characteristic_weight'=>'4.2 kg',
            'characteristic_materials'=>'Seagull waste',
            'image'=>'products\November2022\oogEkfeHfhuZkz0gZ9nU.PNG',          
        ]);
        

        // ? Boxs Seeders

        Product::create([
            
            'name'=>'Seasonal box (Summer)',
            'category_id'=>'6',
            'slug'=>'summer_box',
            'subtitle'=>'Assortment of tools, pots and seeds to start gardening',
            'description'=>'Whether you are a beginner or used to gardening, the Summer boxes allow you to get a large number of tools, pots and products to start your Summer garden. Inside, you will find a transplanter, a pruner, Summer seeds, a book of Summer plants, 2 small pots and a gardener. The season is yours! ',
            'price'=>'11000',
            'characteristic_color'=>'Depend',
            'characteristic_dimension'=>'none',
            'characteristic_weight'=>'none',
            'characteristic_materials'=>'transplanter, pruner, seasonal seeds, seasonal tips book, small pots x2, gardener',
            'image'=>'products\November2022\Eq0CwIeAjPE5xtlkDgRC.PNG',          
        ]);

        Product::create([
            
            'name'=>'Seasonal box (Winter)',
            'category_id'=>'6',
            'slug'=>'winter_box',
            'subtitle'=>'Assortment of tools, pots and seeds to start gardening',
            'description'=>'Whether you are a beginner or used to gardening, the Winter boxes allow you to get a large number of tools, pots and products to start your Winter garden. Inside, you will find a transplanter, a pruner, Winter seeds, a book of Winter plants, 2 small pots and a gardener. The season is yours! ',
            'price'=>'11000',
            'characteristic_color'=>'Depend',
            'characteristic_dimension'=>'none',
            'characteristic_weight'=>'none',
            'characteristic_materials'=>'transplanter, pruner, seasonal seeds, seasonal tips book, small pots x2, gardener',
            'image'=>'products\November2022\jvQuj2G9zzt0CT9ct5UB.PNG',          
        ]);

        Product::create([
            
            'name'=>'Seasonal box (Spring)',
            'category_id'=>'6',
            'slug'=>'spring_box',
            'subtitle'=>'Assortment of tools, pots and seeds to start gardening',
            'description'=>'Whether you are a beginner or used to gardening, the Spring boxes allow you to get a large number of tools, pots and products to start your Spring garden. Inside, you will find a transplanter, a pruner, Spring seeds, a book of Spring plants, 2 small pots and a gardener. The season is yours! ',
            'price'=>'11000',
            'characteristic_color'=>'Depend',
            'characteristic_dimension'=>'none',
            'characteristic_weight'=>'none',
            'characteristic_materials'=>'transplanter, pruner, seasonal seeds, seasonal tips book, small pots x2, gardener',
            'image'=>'products\November2022\6CVpOFRtgcgg6CMjczSJ.PNG',          
        ]);

        Product::create([
            
            'name'=>'Seasonal box (Fall)',
            'category_id'=>'6',
            'slug'=>'fall_box',
            'subtitle'=>'Assortment of tools, pots and seeds to start gardening',
            'description'=>'Whether you are a beginner or used to gardening, the Fall boxes allow you to get a large number of tools, pots and products to start your Fall garden. Inside, you will find a transplanter, a pruner, Fall seeds, a book of Fall plants, 2 small pots and a gardener. The season is yours! ',
            'price'=>'11000',
            'characteristic_color'=>'Depend',
            'characteristic_dimension'=>'none',
            'characteristic_weight'=>'none',
            'characteristic_materials'=>'transplanter, pruner, seasonal seeds, seasonal tips book, small pots x2, gardener',
            'image'=>'products\November2022\v4RuRfULEdw2vWrqq3In.PNG',          
        ]);


    }
}
