<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
    
            'name'=>'Gardening Tools',    
            'slugurl'=>'gardening_tools',  
        ]); 

        Category::create([
    
            'name'=>'Flowers Pots',   
            'slugurl'=>'flowers_pots',    
        ]); 

        Category::create([
    
            'name'=>'Seasonnal Books',  
            'slugurl'=>'seasonnal_books',     
        ]); 

        Category::create([
    
            'name'=>'Fertilizers',  
            'slugurl'=>'fertilizers',     
        ]); 

        Category::create([
    
            'name'=>'Seasonnal Seeds', 
            'slugurl'=>'seasonnal_seeds',      
        ]); 

        Category::create([
    
            'name'=>'Seasonnas Boxs',  
            'slugurl'=>'seasonnas_boxs',     
        ]); 
    }
}
