<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Coupon;

class CouponTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 
        Coupon::create([
    
            'code'=>'audran',
            'percent_off'=>'50',      
        ]); 
        Coupon::create([
    
            'code'=>'mathis',
            'percent_off'=>'10',      
        ]); 
        Coupon::create([
    
            'code'=>'sean',
            'percent_off'=>'20',      
        ]); 
        Coupon::create([
    
            'code'=>'marion',
            'percent_off'=>'5',      
        ]); 

    }
}
