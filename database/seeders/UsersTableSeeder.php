<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'User1',
            'lastname' => 'Next-u',
            'phone_number' => '0643984152',
            'email' => 'user1@email.com',
            'password' => bcrypt('password'),
        ]);
        DB::table('users')->insert([
            'name' => 'user2',
             'lastname' => 'Next-u2',
            'phone_number' => '0678984152',
            'email' => 'user2@email.com',
            'password' => bcrypt('password'),
        ]);
        DB::table('users')->insert([
            'name' => 'admin',
        
             'lastname' => 'Next-u3',
            'phone_number' => '0685741236',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin'),
        ]);
    }
}