<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\ProductsTableSeeder;
use Database\Seeders\CategoriesTableSeeder;
use Database\Seeders\CouponTableSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    $this->call([
        CategoriesTableSeeder::class,
        ProductsTableSeeder::class,
        CouponTableSeeder::class,
        UsersTableSeeder::class,

   ]);
    }
}
