<?php

namespace App\Models;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    public function getEuropePrice()
    {
        $price = $this->price /= 100;
        return number_format($price, 2, ',', ' ') . ' €';
    }

    
    protected $primaryKey = 'id';
     protected $keyType = 'int';

     public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
