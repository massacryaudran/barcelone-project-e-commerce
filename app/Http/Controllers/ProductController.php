<?php

namespace App\Http\Controllers;

use Cart;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\Console\Input\Input;

class ProductController extends Controller
{
    public function index(){
           $products = Product::inRandomOrder()->take(4)->get();

   
        return view('index')->with('products', $products);
        }
        
    

        public function show($slug){

     
        $product = Product::where('slug', $slug)->firstOrFail();
        return view('products.show')->with('product', $product);
        }

        public function category_show(Request $request){
        $products = Product::orderBy('name', 'asc')->get()->where('category_id' , $request->category_id);
        $category = Category::where('id',$request->category_id)->first();
        return view('category.pannel')->with('products', $products)->with('category', $category);
    }

        public function search(){
    
        request()->validate([
        'q' => 'required|min:1'
        ]);

        $q = request()->input('q');

        $products = Product::where('name', 'like', "%$q%")
        ->orWhere('subtitle', 'like', "%$q%")
        ->paginate(6);
    
        $categories = Category::all();
        return view ('products.result_search')->with('products', $products)->with('categories', $categories);                           
    }
    public function category(){
        return view('categories_products');
    }
}
