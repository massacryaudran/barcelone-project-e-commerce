<?php

namespace App\Http\Controllers;

use App\Mail\ContactUsMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SupportController extends Controller
{
        public function support(){
        return view('support');
    }

    public function sendMail(Request $request){
        // dd($request->all());

try {
    $mailData = [
        'firstname' => $request->firstname,
        'lastname' => $request->lastname,
        'email' => $request->email,
        'subject' => $request->subject,
        'message' => $request->message
    ];
    Mail::to($mailData['email'])->send(new ContactUsMail($mailData));

    return redirect()->back()->with('success', 'Email sent successfully !');
}
        catch (\Throwable $th){
            return redirect()->back()->with('error', 'Something went wrong');
        }


    }
}
