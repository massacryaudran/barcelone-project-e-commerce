<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Cart;
use Illuminate\Http\Request;
use App\Models\BoxMaker;

class BoxMakerController extends Controller
{
    public function make_my_box(){

        return view('boxmaker.my_box_maker');
    }
    public function step2(Request $request){

          $boxmaker = new Boxmaker;
          $boxmaker->boxname = $request->boxname;
          $boxmaker->creator = $request->creator;
          $boxmaker->size = $request->size;
          $userId = auth()->user()->id;
          $boxmaker->user_id = $userId; 
          $boxmaker->save();

            // dd($request->all());
       
        return view('boxmaker.my_box_maker_step_2')->with('boxmaker', $boxmaker)->with('userId', $userId);
    }
    public function step3(Request $request){

        $userId = auth()->user()->id;
        $boxmaker = BoxMaker::where('id',$userId)->first();
        $boxmaker->seeds = $request->seeds;
        $boxmaker->books = $request->books;
        $boxmaker->save();
        return view('boxmaker.my_box_maker_step_3')->with('boxmaker', $boxmaker)->with('userId', $userId);
    }
}
