<?php

namespace App\Http\Controllers;

use Cart;
use App\Models\Product;
use App\Models\User;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class AccountController extends Controller
{
    public function index()
    {
        $userId = auth()->user()->id;
        $cartCollection = Cart::session($userId)->getContent();
        $categories = Category::all();


        $products = Product::inRandomOrder()->take(4)->get();
        $users = auth()->user();
        // dd($users);
        return view('account.index')->with('products', $products)->with('cartCollection', $cartCollection)
        ->with('categories', $categories)->with('users', $users);
    }

      public function modify(Request $request)
      {
          $messages = [
              'required' => 'Le champ ":attribute" est manquant',
              'string' => ':attribute n\'est pas valide',
              'integer' => ':attribute n\'est pas valide',
            ];

          $attributes = [
            'name' => 'name',
            'lastname' => 'last name',
            'email' => 'Email address',
            'phone_number' => 'Phone number',
          ];

          $validateData = Validator::make($request->all(), [
            'name' => 'required | string',
            'lastname' => 'required | string',
            'phone_number' => 'required | digits:10',
            'email' => 'required | email',
 
          ], $messages, $attributes);

          if ($validateData->fails()) {
              $errors = $validateData->errors();
              foreach ($errors->all() as $message) {
                  return back()->with('success', $message);
              }
              return redirect(url()->previous());
          }

          $user = User::where('id', $request->id)->first();
          if ($user) {
              $user->name = $request->name;
              $user->lastname = $request->lastname;
              $user->email = $request->email;
              $user->phone_number = $request->phone_number;
              $user->save();
          } else {
               return back(); 
          }
          return redirect('/account');
      }

      public function del($id){
        $user = User::where('id', $id)->first();
        if ($user) {
            $user->delete();
              return back()->with('success', 'Your account has been removed');
        }
        else {
           return back()->with('success', 'Hoh no something went wrong');
        }
        return redirect('/');
      }

}
