<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

     public function about_us(){
     
        return view('about');
    }
    public function legal_mention(){
     
        return view('mention');
    }
    public function how_to_start(){
           $category = Category::all();
        return view('tutorial')->with('category', $category);
    }
}
