<?php

namespace App\Http\Controllers;

use Cart;
use App\Models\Coupon;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    if (Auth::check()) {
        $userId = auth()->user()->id;
        $quantity =$request->quantity;
        $product = Product::find($request->product_id);
        Cart::session($userId)->add($product->id, $product->name, $product->price, $quantity)
        ->associate('App\Models\Product');

        return redirect()->route('product.pannel')->with('success', 'Product has been added');
    } else {
        $quantity =$request->quantity;
        $product = Product::find($request->product_id);
        Cart::add($product->id, $product->name, $product->price, $quantity)
        ->associate('App\Models\Product');

        return redirect()->route('product.pannel')->with('success', 'Product has been added');
    }
}
// Cart::add($request->id, $request->title, 1, $request->price)
//             ->associate('App\Product');
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
    if (Auth::check()) {

        $userId = auth()->user()->id;
        $cartCollection = Cart::session($userId)->getContent();
        $categories = Category::all();

        //  * Tax condition
        $condition = new \Darryldecode\Cart\CartCondition(array(
        'name' => 'VAT 12.5%',
        'type' => 'tax',
        'target' => 'total', // this condition will be applied to cart's subtotal when getSubTotal() is called.
        'value' => '20%',
        'attributes' => array( // attributes field is optional
            'description' => 'Value added tax',
            'more_data' => 'more data here'
        )
        ));

        // * Shipping condition
        $condition2 = new \Darryldecode\Cart\CartCondition(array(
        'name' => 'Express Shipping $5',
        'type' => 'shipping',
        'target' => 'total', // this condition will be applied to cart's subtotal when getSubTotal() is called.
        'value' => '+500',

      
        ));
        
        
        Cart::session($userId)->condition($condition);
        Cart::session($userId)->condition($condition2);

        $subTotal = Cart::getSubTotal();
        $condition = Cart::getCondition('VAT 12.5%');
        $condition2 = Cart::getCondition('Express Shipping $5');

        $conditionCalculatedValue = $condition->getCalculatedValue($subTotal);
        $conditionCalculatedValue2 = $condition2->getCalculatedValue($subTotal);
  // * Coupon

        $coupon101 = new \Darryldecode\Cart\CartCondition(array(
        'name' => 'coupon101',
        'type' => 'coupon',
        'target' => 'total',
        'value' => '-50%'));
         
        $coupondonne = Cart::getCondition('coupon101');
    
        if(is_null($coupondonne)) {
        $conditionCalculatedValue3 = 0;
        }
        else {
        $conditionCalculatedValue3 = $coupondonne->getCalculatedValue($subTotal);
        }

        return view('cart.index')->with('cartCollection', $cartCollection)->with('conditionCalculatedValue', $conditionCalculatedValue)->with('categories', $categories)
        ->with('conditionCalculatedValue2', $conditionCalculatedValue2)->with('conditionCalculatedValue3', $conditionCalculatedValue3);
    }
    else {
       
        $cartCollection = Cart::getContent();
        $categories = Category::all();

        //  * Tax condition
        $condition = new \Darryldecode\Cart\CartCondition(array(
        'name' => 'VAT 12.5%',
        'type' => 'tax',
        'target' => 'total', // this condition will be applied to cart's subtotal when getSubTotal() is called.
        'value' => '20%',
        'attributes' => array( // attributes field is optional
            'description' => 'Value added tax',
            'more_data' => 'more data here'
        )
        ));

        // * Shipping condition
        $condition2 = new \Darryldecode\Cart\CartCondition(array(
        'name' => 'Express Shipping $5',
        'type' => 'shipping',
        'target' => 'total', // this condition will be applied to cart's subtotal when getSubTotal() is called.
        'value' => '+500',
        
        ));
       
        Cart::condition($condition);
        Cart::condition($condition2);

        $subTotal = Cart::getSubTotal();
        $condition = Cart::getCondition('VAT 12.5%');
        $condition2 = Cart::getCondition('Express Shipping $5');

        $conditionCalculatedValue = $condition->getCalculatedValue($subTotal);
        $conditionCalculatedValue2 = $condition2->getCalculatedValue($subTotal);
  // * Coupon

        $coupon101 = new \Darryldecode\Cart\CartCondition(array(
        'name' => 'coupon101',
        'type' => 'coupon',
        'target' => 'total',
        'value' => '-50%'));
         
        $coupondonne = Cart::getCondition('coupon101');
    
        if(is_null($coupondonne)) {
        $conditionCalculatedValue3 = 0;
        }
        else {
        $conditionCalculatedValue3 = $coupondonne->getCalculatedValue($subTotal);
        }

        return view('cart.index')->with('cartCollection', $cartCollection)->with('conditionCalculatedValue', $conditionCalculatedValue)->with('categories', $categories)
        ->with('conditionCalculatedValue2', $conditionCalculatedValue2)->with('conditionCalculatedValue3', $conditionCalculatedValue3);

}
}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function emptycart()
    {
    if (Auth::check()) {
        $userId = auth()->user()->id;
        Cart::session($userId)->clear();
        return back()->with('succes', 'The cart is empty');
    }
    else{
    Cart::clear();
    return back()->with('succes', 'The cat is empty');
        }
        }
    public function destroy($rowId)
    {
if (Auth::check()) {
    $userId = auth()->user()->id;
    Cart::session($userId)->remove($rowId);
    return back()->with('success', 'Product has been removed');
}
         else{
       
    Cart::remove($rowId);
    return back()->with('success', 'Product has been removed');
         }
    }

    public function storeCoupon(Request $request){

        // $code = $request->get('code');
        $code = ['code' =>$request->code];
        
        $coupon = Coupon::where('code' , $code)->first();

        if (!$coupon){
            return redirect()->back()->with('error', 'The coupon is not valid');
        }
        $userId = auth()->user()->id;

        $coupon101 = new \Darryldecode\Cart\CartCondition(array(
        'name' => 'coupon101',
        'type' => 'coupon',
        'target' => 'total',
        'value' => '-50%'
    ));
        $coupondata = Cart::session($userId)->condition($coupon101);
        return redirect()->back()->with('success', 'Coupon has been applied');
        
    }
    public function destroyCoupon() {

    $userId = auth()->user()->id;
    $conditionName = 'coupon101';
        
    Cart::session($userId)->removeCartCondition($conditionName);
     return redirect()->back()->with('success', 'Coupon has been deleted');

    }

    public function update_quantity(Request $request, $powId){
   
    $value = $request->quantity;
    // dd($value);
    dd($request->all());
    Cart::update($powId, array(
    'quantity' => array(
        'relative' => false,
        'value' => $value['qty']
    ),
));

    return redirect()->back();
    }
}