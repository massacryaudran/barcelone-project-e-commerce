<?php

namespace App\Http\Controllers;
use Cart;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 


class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    
if (Auth::check()) {
    $userId = auth()->user()->id;
    $cartCollection = Cart::session($userId)->getContent();
    $categories = Category::all();
}
   else{
     $cartCollection = Cart::getContent();
    $categories = Category::all();
   }
   
        return view('checkout.index')->with('cartCollection', $cartCollection)
        ->with('categories', $categories);
    }

}
