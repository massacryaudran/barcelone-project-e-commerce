<?php

namespace App\Providers;

use Cart;
use App\Models\Product;
use App\Models\Category;
use View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', function($view){
        
        if (Auth::check()) {
            $userId = auth()->user()->id;
            $cartCollection = Cart::session($userId)->getContent();
            $categories = Category::all();
            $view->with('cartCollection', $cartCollection)->with('categories', $categories);
        } else {
            // return redirect()->route('login');
            $cartCollection = Cart::getContent();
            $categories = Category::all();
            $view->with('cartCollection', $cartCollection)->with('categories', $categories);
        }
    });
    }
}
