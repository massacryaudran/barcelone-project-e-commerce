import React, { useRef } from "react";
import { useGLTF } from "@react-three/drei";

export default function Model(props) {
    const { nodes, materials } = useGLTF(
        "/storage/objects/box_maker/box/box.gltf"
    );
    return (
        <group {...props} dispose={null}>
            <mesh
                geometry={nodes.Box_Base.geometry}
                material={materials["Material.003"]}
                position={[-1.99, 0.23, -0.57]}
                rotation={[Math.PI / 2, 0, 0]}
                scale={0.04}
            >
                <mesh
                    geometry={nodes.Box_faces.geometry}
                    material={nodes.Box_faces.material}
                />
                <mesh
                    geometry={nodes.Text.geometry}
                    material={materials["Material.001"]}
                    position={[69.98, 38.15, -12.13]}
                    rotation={[0, 0, -0.06]}
                    scale={[5.96, 5.34, 5.33]}
                />
            </mesh>
            <mesh
                geometry={nodes.Box_Base001.geometry}
                material={materials["Material.006"]}
                position={[-1.99, 0.23, -0.57]}
                rotation={[Math.PI / 2, 0, 0]}
                scale={0.04}
            />
        </group>
    );
}

useGLTF.preload("/storage/objects/box_maker/box/box.gltf");
