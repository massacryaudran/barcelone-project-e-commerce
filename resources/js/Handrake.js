import React, { useRef } from "react";
import { useGLTF } from "@react-three/drei";

export default function Handrake(props) {
    const { nodes, materials } = useGLTF(
        "/storage/objects/tools/hand_rake/hand_rake.gltf"
    );
    return (
        <group {...props} dispose={null}>
            <group
                position={[0, 0, 0]}
                rotation={[Math.PI / 2, 0, 1.53]}
                scale={[0.4, 0.4, 0.4]}
            >
                <mesh
                    geometry={nodes["Bunty-G002_1"].geometry}
                    material={materials.Material}
                />
                <mesh
                    geometry={nodes["Bunty-G002_2"].geometry}
                    material={materials.garden_tools}
                />
            </group>
        </group>
    );
}

useGLTF.preload("/storage/objects/tools/hand_rake/hand_rake.gltf");
