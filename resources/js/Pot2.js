import React, { useRef } from "react";
import { useGLTF } from "@react-three/drei";

export default function Model(props) {
    const { nodes, materials } = useGLTF(
        "/storage/objects/pots/pot2/pot2.gltf"
    );
    return (
        <group {...props} dispose={null}>
            <mesh geometry={nodes.Pot.geometry} material={materials.Concrete} />
            <mesh
                geometry={nodes.Ground.geometry}
                material={materials.Ground}
                position={[0, 0.42, -0.01]}
                scale={1.05}
            />
            <mesh
                geometry={nodes.Leaf.geometry}
                material={materials.Leaf}
                position={[0.25, 0.93, -0.04]}
                rotation={[3.03, 0.41, -2.88]}
                scale={0.02}
            />
            <mesh
                geometry={nodes.Tree.geometry}
                material={materials.Tree}
                position={[0.23, 0.8, -0.01]}
                rotation={[0.02, 0.1, -0.66]}
                scale={0.03}
            />
            <mesh
                geometry={nodes.Text.geometry}
                material={materials["Material.001"]}
                position={[0.77, 0.19, 0.21]}
                rotation={[Math.PI / 2, -0.18, -Math.PI / 2]}
            />
        </group>
    );
}

useGLTF.preload("/storage/objects/pots/pot2/pot2.gltf");
