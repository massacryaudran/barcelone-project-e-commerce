import React from "react";
import { Html } from "@react-three/drei";
import Nav from "react-bootstrap/esm/Nav";

export default function Marker(_a) {
    var position = _a.position,
        name = _a.name,
        id = _a.id,
        onMarkerClicked = _a.onMarkerClicked;
    return (
        <mesh position={position}>
            <Html scaleFactor={100}>
                <div
                    style={{
                        display: "flex",
                        flexDirection: "row",
                    }}
                    onClick={() => onMarkerClicked(id)}
                >
                    <div
                        style={{
                            border: "1px solid black",
                            borderRadius: "10px",
                            width: "1em",
                            height: "fit-content",
                            textAlign: "center",
                            fontSize: "1em",
                            fontFamily: "Antonio, sans-serif",
                            color: "black",
                            fontWeight: "bold",
                        }}
                    >
                        {id}
                    </div>
                    <div className="box">
                        <Nav.Link
                            style={{
                                textAlign: "center",
                                fontSize: "1.5em",
                                fontFamily: "Antonio, sans-serif",
                                color: "black",
                                textDecoration: "underline #9d5dea",
                            }}
                        >
                            {name}
                        </Nav.Link>
                    </div>
                </div>
            </Html>
        </mesh>
    );
}
