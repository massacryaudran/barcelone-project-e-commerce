import React, { useRef } from "react";
import { useGLTF } from "@react-three/drei";

export default function Model(props) {
    const { nodes, materials } = useGLTF(
        "/storage/objects/category/category.gltf"
    );
    return (
        <group {...props} dispose={null}>
            <group
                position={[35, 0, -28]}
                rotation={[Math.PI / 2, 0, 0]}
                scale={1}
            >
                <mesh
                    geometry={nodes.Mesh010.geometry}
                    material={materials.leaves10}
                />
                <mesh
                    geometry={nodes.Mesh010_1.geometry}
                    material={materials.PLANT02}
                />
                <mesh
                    geometry={nodes.Mesh010_2.geometry}
                    material={materials.pot_m}
                />
                <mesh
                    geometry={nodes.Mesh010_3.geometry}
                    material={materials.plank_box}
                />
                <mesh
                    geometry={nodes.Mesh010_4.geometry}
                    material={materials.shovel_m}
                />
                <mesh
                    geometry={nodes.Mesh010_5.geometry}
                    material={materials["Black plastic old scratched"]}
                />
                <mesh
                    geometry={nodes.Mesh010_6.geometry}
                    material={materials.PLANT01}
                />
                <mesh
                    geometry={nodes.Mesh010_7.geometry}
                    material={materials.dirt}
                />
                <mesh
                    geometry={nodes.Mesh010_8.geometry}
                    material={materials.pot}
                />
                <mesh
                    geometry={nodes.Mesh010_9.geometry}
                    material={materials["Material.001"]}
                />
                <mesh
                    geometry={nodes.Mesh010_10.geometry}
                    material={materials["Scene_-_Root"]}
                />
                <mesh
                    geometry={nodes.Mesh010_11.geometry}
                    material={materials.lambert1}
                />
                <mesh
                    geometry={nodes.Mesh010_12.geometry}
                    material={materials.leaves1}
                />
                <mesh
                    geometry={nodes.Mesh010_13.geometry}
                    material={materials.leaves2}
                />
                <mesh
                    geometry={nodes.Mesh010_14.geometry}
                    material={materials.leaves3}
                />
                <mesh
                    geometry={nodes.Mesh010_15.geometry}
                    material={materials.leaves5}
                />
                <mesh
                    geometry={nodes.Mesh010_16.geometry}
                    material={materials.leaves6}
                />
                <mesh
                    geometry={nodes.Mesh010_17.geometry}
                    material={materials.leaves9}
                />
                <mesh
                    geometry={nodes.Mesh010_18.geometry}
                    material={materials.Material}
                />
            </group>
        </group>
    );
}

useGLTF.preload("/storage/objects/category/category.gltf");
