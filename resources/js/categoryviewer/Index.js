import React, { useState, Suspense } from "react";
import { Canvas, useThree } from "@react-three/fiber";
import {
    OrbitControls,
    Stars,
    Environment,
    Sky,
    useProgress,
    Html,
} from "@react-three/drei";
import { animated, useSpring, config } from "@react-spring/web";

import { createRoot } from "react-dom/client";
import Nav from "react-bootstrap/esm/Nav";

if (document.getElementById("category")) {
    var category = "category";

    var Planer = React.lazy(() => import("./Category"));
    var Leaves = React.lazy(() => import("./Scene"));
    var Marker = React.lazy(() => import("./marker"));
}
function Loader() {
    const { progress } = useProgress();
    return (
        <Html
            center
            style={{
                zIndex: "100",
                position: "absolute",
                top: "50%",
                left: "50%",
            }}
        >
            {progress} % loaded
        </Html>
    );
}
function Navigation(_a) {
    var _b;
    var cameraPosition = _a.cameraPosition;
    var cameraRotation = _a.cameraRotation;
    var camera = useThree().camera;
    (_b = camera.position).set.apply(_b, cameraPosition);
    (_b = camera.rotation).set.apply(_b, cameraRotation);
    return null;
}

var selectedItemIndex;
let initialCameraPos;

// Changing the camera position if on mobile or on desktop
function readSize() {
    const width = window.innerWidth;
    const height = window.innerHeight;
    console.log(width);
    console.log(height);
    if (width < 1080 || height < 900) {
        initialCameraPos = [-110, 80, 82];
    } else {
        initialCameraPos = [-70, 50, 32];
    }
}
readSize();
window.addEventListener("resize", readSize);

var initialCameraRot = [0, 0, 0];
var initialControlsTarget = [0, 0, 0];
function CatApp() {
    var markers = (0, useState)([
        {
            position: [-100, 45, 38],
            cameraPos: [-100, 45, 38],

            name: "Title",
        },
        {
            position: [10, 13, -12],
            cameraPos: [15, 8, 5],
            rotationPos: [0, 20, 0],
            name: "Pots",
            content:
                "What could be better than a beautiful recycled pot to plant your own seeds! Check out our latest pots!",
            href: "flowers_pots",
        },
        {
            position: [10, 20, 19],
            cameraPos: [-7, 8, 10],
            rotationPos: [0, 20, 0],
            name: "Tools",
            href: "gardening_tools",
            content:
                "Discover our special beginner gardening tools. They are compact and, at the same time, essential to start your gardening adventure. Made of recycled material and ensuring a perfect second life.",
        },
        {
            position: [0, 10, 0],
            cameraPos: [-7, 8, 10],
            rotationPos: [0, 20, 0],
            name: "Fertilizers",
            href: "fertilizers",
            content:
                "A little boost to allow your plants to grow faster while remaining pesticide-free .",
        },
        {
            position: [-35, 13, -7],
            cameraPos: [0, 8, -10],
            rotationPos: [20, 20, 0],
            name: "Seeds",
            href: "seasonnal_seed",
            content:
                "Get ready to order our special seeds that grow at home ! Discover many seasonal fruits and vegetables at a reduced price and accompanied by a recycled storage bag",
        },

        {
            position: [0, 15, -20],
            cameraPos: [-20, 8, 0],
            rotationPos: [20, 20, 0],
            name: "Books",
            href: "seasonnal_books",
            content:
                "A thirst for discovery ! Learn a lot with our special home gardening books. A book for each season is available !",
        },
        {
            position: [-25, 10, 23],
            cameraPos: [-7, 8, 10],
            rotationPos: [0, 20, 0],
            name: "Boxs",
            href: "seasonnal_boxs",
            content:
                "Need everything at once? Our homemade gardening boxes are perfect for getting started and beginning an adventure full of surprises! A book, tools, seeds and pots are provided.",
        },
    ])[0];
    const [isAnimating, setIsAnimating] = useState(false);
    var AnimatedNavigation = animated(Navigation);
    var AnimatedOrbitControls = animated(OrbitControls);
    var _b = (0, useState)({
            cachedPos: initialCameraPos,
            cachedRot: initialCameraRot,
            cachedTarget: initialControlsTarget,
            pos: initialCameraPos,
            rot: initialCameraRot,
            target: initialControlsTarget,
            autoRotate: true,
        }),
        cameraValues = _b[0],
        setCameraValues = _b[1];
    function onNavigationItemClicked(id) {
        if (selectedItemIndex !== id && !isAnimating) {
            selectedItemIndex = id;
            setIsAnimating(true);

            setCameraValues({
                cachedPos: cameraValues.pos,
                cachedTarget: cameraValues.cachedTarget,
                pos: markers[selectedItemIndex].cameraPos,
                rot: markers[selectedItemIndex].rotationPos,
                target: markers[selectedItemIndex].position,
                autoRotate: id === 0,
            });
            setContent(markers[id].content);
            setHref(markers[id].href);
            setDisplay(true);
        }
    }
    var spring = (0, useSpring)({
        pos: cameraValues.pos,
        target: cameraValues.target,
        from: {
            pos: cameraValues.cachedPos,
            target: cameraValues.cachedTarget,
        },
        config: config.slow,
        onRest: function () {
            return setIsAnimating(false);
        },
    });
    const [content, setContent] = useState("");
    const [href, setHref] = useState("");
    const [isDisplay, setDisplay] = useState(false);

    return (
        <div
            className="content"
            style={{ width: "100%", height: "100%", position: "absolute" }}
        >
            <div
                className="ui"
                style={{
                    position: "absolute",
                    zIndex: 100,
                    top: "50%",
                    transform: "translateY(-50%)",
                }}
            >
                <div className="nav_link">
                    <Nav
                        defaultActiveKey="/home"
                        style={{
                            display: "flex",
                            flexDirection: "column",
                            color: "black !important",
                            fontSize: "35pt",
                            fontFamily: "Antonio, sans-serif",
                            filter: "drop-shadow(0px 0px 10px grey)",
                            gap: "20px",
                            marginLeft: "20px",
                        }}
                    >
                        <Nav.Link onClick={() => onNavigationItemClicked(1)}>
                            {markers[1].name}
                        </Nav.Link>
                        <Nav.Link onClick={() => onNavigationItemClicked(2)}>
                            {markers[2].name}
                        </Nav.Link>
                        <Nav.Link onClick={() => onNavigationItemClicked(3)}>
                            {markers[3].name}
                        </Nav.Link>
                        <Nav.Link onClick={() => onNavigationItemClicked(4)}>
                            {markers[4].name}
                        </Nav.Link>
                        <Nav.Link onClick={() => onNavigationItemClicked(5)}>
                            {markers[5].name}
                        </Nav.Link>
                        <Nav.Link onClick={() => onNavigationItemClicked(6)}>
                            {markers[6].name}
                        </Nav.Link>
                    </Nav>
                </div>
            </div>
            {isDisplay && (
                <div
                    className="backdrop_text"
                    style={{
                        position: "absolute",
                        top: "40%",
                        zIndex: "20",
                        left: "20%",
                        borderRadius: "20px",
                        display: "flex",
                        justifyContent: "center",
                        flexDirection: "column",
                        width: "400px",
                        height: "fit-content",
                        padding: "1em",
                    }}
                >
                    <span
                        className="infoproduct"
                        style={{
                            fontSize: "25px",
                            textAlign: "justify",
                            fontFamily: "Antonio, sans-serif",
                        }}
                    >
                        {content}
                    </span>
                    <br></br>
                    <button
                        style={{
                            background: " #9b59b6 ",
                            border: " 1px solid transparent",
                            borderRadius: "5px",
                            margin: "auto",
                            width: "fit-content",
                            marginTop: "10px",
                            padding: "0.3em",
                        }}
                    >
                        <label
                            htmlFor={href}
                            style={{
                                fontSize: "20px",
                                color: "black",
                                textAlign: "center",
                                fontWeight: "bold",
                                textDecoration: "none",
                                cursor: "pointer",
                            }}
                        >
                            Discover
                        </label>
                    </button>
                </div>
            )}

            <React.StrictMode>
                <Canvas
                    camera={{ position: cameraValues.pos, rotation: [0, 0, 0] }}
                >
                    <ambientLight />

                    <AnimatedNavigation
                        cameraPosition={spring.pos}
                        cameraRotation={spring.rot}
                    />

                    <Suspense fallback={<Loader />}>
                        <Planer position={(0, 0, 0)} scale={0.5} />

                        {isAnimating ? null : (
                            <group>
                                <Marker
                                    position={markers[1].position}
                                    rotation={markers[1].rotation}
                                    name={markers[1].name}
                                    id={1}
                                    onMarkerClicked={onNavigationItemClicked}
                                />
                                <Marker
                                    position={markers[2].position}
                                    rotation={markers[2].rotation}
                                    name={markers[2].name}
                                    id={2}
                                    onMarkerClicked={onNavigationItemClicked}
                                />
                                <Marker
                                    position={markers[3].position}
                                    rotation={markers[3].rotation}
                                    name={markers[3].name}
                                    id={3}
                                    onMarkerClicked={onNavigationItemClicked}
                                />
                                <Marker
                                    position={markers[4].position}
                                    rotation={markers[4].rotation}
                                    name={markers[4].name}
                                    id={4}
                                    onMarkerClicked={onNavigationItemClicked}
                                />
                                <Marker
                                    position={markers[5].position}
                                    rotation={markers[5].rotation}
                                    name={markers[5].name}
                                    id={5}
                                    onMarkerClicked={onNavigationItemClicked}
                                />
                                <Marker
                                    position={markers[6].position}
                                    rotation={markers[6].rotation}
                                    name={markers[6].name}
                                    id={6}
                                    onMarkerClicked={onNavigationItemClicked}
                                />
                            </group>
                        )}
                    </Suspense>
                    <group position={[0, -70, 0]}>
                        <Leaves />
                    </group>
                    <AnimatedOrbitControls
                        autoRotate={cameraValues.autoRotate}
                        autoRotateSpeed={0.2}
                        maxPolarAngle={Math.PI / 2.5}
                        minPolarAngle={Math.PI / 3}
                        target={spring.target}
                        enableKeys={false}
                        enablePan={false}
                    />
                    <Stars
                        radius={100}
                        depth={100}
                        count={2000}
                        factor={6}
                        saturation={0}
                        fade={true}
                    />
                    <Environment
                        files="/storage/objects/landing/adamsbridge.hdr"
                        blur={1}
                    />

                    <Sky
                        distance={450000}
                        sunPosition={[0, 1, 0]}
                        inclination={0}
                        azimuth={0.25}
                    />
                </Canvas>
            </React.StrictMode>
        </div>
    );
}

export default CatApp;

try {
    const container = document.getElementById(category);
    const root = createRoot(container);
    root.render(<CatApp />);
} catch (error) {}
