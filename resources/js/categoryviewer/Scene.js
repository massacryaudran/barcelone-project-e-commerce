import React, { useRef, useEffect } from "react";
import { useGLTF, useAnimations } from "@react-three/drei";

export default function Model(props) {
    const group = useRef();
    const { nodes, materials, animations } = useGLTF(
        "/storage/objects/category/animation_leaves/scene.gltf"
    );
    const { actions, names } = useAnimations(animations, group);

    useEffect(() => {
        // Reset and fade in animation after an index has been changed
        actions[names].reset().fadeIn(0.5).play();
        // In the clean-up phase, fade it out
        return () => actions[names].fadeOut(0.5);
    }, [actions, names]);
    return (
        <group ref={group} {...props} dispose={null}>
            <group name="Sketchfab_Scene">
                <group name="Sketchfab_model" rotation={[-Math.PI / 2, 0, 0]}>
                    <group name="root">
                        <group
                            name="GLTF_SceneRootNode"
                            rotation={[Math.PI / 2, 0, 0]}
                        >
                            <group name="RootNode0_0" scale={2}>
                                <group name="skeletal22_22">
                                    <group name="GLTF_created_0">
                                        <primitive
                                            object={
                                                nodes.GLTF_created_0_rootJoint
                                            }
                                        />
                                        <group name="falling_leaves_12_2_correction">
                                            <group name="falling_leaves_12_2" />
                                        </group>
                                        <group name="falling_leaves_23_3_correction">
                                            <group name="falling_leaves_23_3" />
                                        </group>
                                        <group name="falling_leaves_34_4_correction">
                                            <group name="falling_leaves_34_4" />
                                        </group>
                                        <group name="falling_leaves_45_5_correction">
                                            <group name="falling_leaves_45_5" />
                                        </group>
                                        <group name="falling_leaves_56_6_correction">
                                            <group name="falling_leaves_56_6" />
                                        </group>
                                        <group name="falling_leaves_67_7_correction">
                                            <group name="falling_leaves_67_7" />
                                        </group>
                                        <group name="falling_leaves_78_8_correction">
                                            <group name="falling_leaves_78_8" />
                                        </group>
                                        <group name="falling_leaves_89_9_correction">
                                            <group name="falling_leaves_89_9" />
                                        </group>
                                        <group name="falling_leaves_910_10_correction">
                                            <group name="falling_leaves_910_10" />
                                        </group>
                                        <group name="falling_leaves_1011_11_correction">
                                            <group name="falling_leaves_1011_11" />
                                        </group>
                                        <group name="falling_leaves_1112_12_correction">
                                            <group name="falling_leaves_1112_12" />
                                        </group>
                                        <group name="falling_leaves_1213_13_correction">
                                            <group name="falling_leaves_1213_13" />
                                        </group>
                                        <group name="falling_leaves_1314_14_correction">
                                            <group name="falling_leaves_1314_14" />
                                        </group>
                                        <group name="falling_leaves_1415_15_correction">
                                            <group name="falling_leaves_1415_15" />
                                        </group>
                                        <group name="falling_leaves_1516_16_correction">
                                            <group name="falling_leaves_1516_16" />
                                        </group>
                                        <group name="falling_leaves_1617_17_correction">
                                            <group name="falling_leaves_1617_17" />
                                        </group>
                                        <group name="falling_leaves_1718_18_correction">
                                            <group name="falling_leaves_1718_18" />
                                        </group>
                                        <group name="falling_leaves_1819_19_correction">
                                            <group name="falling_leaves_1819_19" />
                                        </group>
                                        <group name="falling_leaves_1920_20_correction">
                                            <group name="falling_leaves_1920_20" />
                                        </group>
                                        <group name="falling_leaves_2021_21_correction">
                                            <group name="falling_leaves_2021_21" />
                                        </group>
                                        <skinnedMesh
                                            name="Object_30"
                                            geometry={nodes.Object_30.geometry}
                                            material={materials.material_0}
                                            skeleton={nodes.Object_30.skeleton}
                                        />
                                        <skinnedMesh
                                            name="Object_33"
                                            geometry={nodes.Object_33.geometry}
                                            material={materials.material_1}
                                            skeleton={nodes.Object_33.skeleton}
                                        />
                                        <skinnedMesh
                                            name="Object_36"
                                            geometry={nodes.Object_36.geometry}
                                            material={materials.material_2}
                                            skeleton={nodes.Object_36.skeleton}
                                        />
                                        <skinnedMesh
                                            name="Object_39"
                                            geometry={nodes.Object_39.geometry}
                                            material={materials.material_3}
                                            skeleton={nodes.Object_39.skeleton}
                                        />
                                        <skinnedMesh
                                            name="Object_42"
                                            geometry={nodes.Object_42.geometry}
                                            material={materials.material_4}
                                            skeleton={nodes.Object_42.skeleton}
                                        />
                                        <skinnedMesh
                                            name="Object_45"
                                            geometry={nodes.Object_45.geometry}
                                            material={materials.material_5}
                                            skeleton={nodes.Object_45.skeleton}
                                        />
                                        <skinnedMesh
                                            name="Object_48"
                                            geometry={nodes.Object_48.geometry}
                                            material={materials.material_6}
                                            skeleton={nodes.Object_48.skeleton}
                                        />
                                        <skinnedMesh
                                            name="Object_51"
                                            geometry={nodes.Object_51.geometry}
                                            material={materials.material_7}
                                            skeleton={nodes.Object_51.skeleton}
                                        />
                                        <skinnedMesh
                                            name="Object_54"
                                            geometry={nodes.Object_54.geometry}
                                            material={materials.material_8}
                                            skeleton={nodes.Object_54.skeleton}
                                        />
                                        <skinnedMesh
                                            name="Object_57"
                                            geometry={nodes.Object_57.geometry}
                                            material={materials.material_9}
                                            skeleton={nodes.Object_57.skeleton}
                                        />
                                        <skinnedMesh
                                            name="Object_60"
                                            geometry={nodes.Object_60.geometry}
                                            material={materials.material_10}
                                            skeleton={nodes.Object_60.skeleton}
                                        />
                                        <skinnedMesh
                                            name="Object_63"
                                            geometry={nodes.Object_63.geometry}
                                            material={materials.material_11}
                                            skeleton={nodes.Object_63.skeleton}
                                        />
                                        <skinnedMesh
                                            name="Object_66"
                                            geometry={nodes.Object_66.geometry}
                                            material={materials.material_12}
                                            skeleton={nodes.Object_66.skeleton}
                                        />
                                        <skinnedMesh
                                            name="Object_69"
                                            geometry={nodes.Object_69.geometry}
                                            material={materials.material_13}
                                            skeleton={nodes.Object_69.skeleton}
                                        />
                                        <skinnedMesh
                                            name="Object_72"
                                            geometry={nodes.Object_72.geometry}
                                            material={materials.material_14}
                                            skeleton={nodes.Object_72.skeleton}
                                        />
                                        <skinnedMesh
                                            name="Object_75"
                                            geometry={nodes.Object_75.geometry}
                                            material={materials.material_15}
                                            skeleton={nodes.Object_75.skeleton}
                                        />
                                        <skinnedMesh
                                            name="Object_78"
                                            geometry={nodes.Object_78.geometry}
                                            material={materials.material_16}
                                            skeleton={nodes.Object_78.skeleton}
                                        />
                                        <skinnedMesh
                                            name="Object_81"
                                            geometry={nodes.Object_81.geometry}
                                            material={materials.material_17}
                                            skeleton={nodes.Object_81.skeleton}
                                        />
                                        <skinnedMesh
                                            name="Object_84"
                                            geometry={nodes.Object_84.geometry}
                                            material={materials.material_18}
                                            skeleton={nodes.Object_84.skeleton}
                                        />
                                        <skinnedMesh
                                            name="Object_87"
                                            geometry={nodes.Object_87.geometry}
                                            material={materials.material_19}
                                            skeleton={nodes.Object_87.skeleton}
                                        />
                                    </group>
                                </group>
                            </group>
                        </group>
                    </group>
                </group>
            </group>
        </group>
    );
}

useGLTF.preload("/storage/objects/category/animation_leaves/scene.gltf");
