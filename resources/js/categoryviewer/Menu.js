import React from "react";
import Nav from "react-bootstrap/Nav";

export default function Menu(_a) {
    var items = _a.items,
        onMarkerClicked = _a.onMarkerClicked,
        onTitleClicked = _a.onTitleClicked;
    return (
        <div className="ui">
            <h2
                className="title"
                onClick={function () {
                    return onTitleClicked();
                }}
            >
                TEST
            </h2>
            <Nav defaultActiveKey="/home" className="flex-column">
                {items.map((marker) => {
                    let key = items.indexOf(marker);
                    let id = key + 1;
                    return (
                        <Nav.Link onClick={() => onMarkerClicked(id)} key={key}>
                            {marker.name}
                        </Nav.Link>
                    );
                })}
            </Nav>
        </div>
    );
}
