import React, { useEffect } from "react";
import { Suspense } from "react";
import { PerspectiveCamera } from "three";
import { Canvas } from "@react-three/fiber";
import { createRoot } from "react-dom/client";
import { Html, useProgress } from "@react-three/drei";

import {
    BakeShadows,
    OrbitControls,
    Stage,
    Icosahedron,
} from "@react-three/drei";

// Condition to load the good object in the page
// Tools
if (document.getElementById("hand_rake")) {
    var scene = "hand_rake";
    var Object = React.lazy(() => import("../Handrake.js"));
} else if (document.getElementById("watering_can")) {
    var scene = "watering_can";
    var Object = React.lazy(() => import("../Wateringcan.js"));
} else if (document.getElementById("transplanter")) {
    var scene = "transplanter";
    var Object = React.lazy(() => import("../Transplanter.js"));
} else if (document.getElementById("shovel")) {
    var scene = "shovel";
    var Object = React.lazy(() => import("../Shovel.js"));
}
// Pots
else if (document.getElementById("gardener")) {
    var scene = "gardener";
    var Object = React.lazy(() => import("../Pot1.js"));
} else if (document.getElementById("small_pot")) {
    var scene = "small_pot";
    var Object = React.lazy(() => import("../Pot2.js"));
} else if (document.getElementById("large_pot")) {
    var scene = "large_pot";
    var Object = React.lazy(() => import("../Pot3.js"));
}
// Books
else if (document.getElementById("spring_book")) {
    var scene = "spring_book";
    var Object = React.lazy(() => import("../Book.js"));
} else if (document.getElementById("summer_book")) {
    var scene = "summer_book";
    var Object = React.lazy(() => import("../Book.js"));
} else if (document.getElementById("winter_book")) {
    var scene = "winter_book";
    var Object = React.lazy(() => import("../Book.js"));
} else if (document.getElementById("fall_book")) {
    var scene = "fall_book";
    var Object = React.lazy(() => import("../Book.js"));
}
// Seeds
else if (document.getElementById("spring_seeds")) {
    var scene = "spring_seeds";
    var Object = React.lazy(() => import("../Seed.js"));
} else if (document.getElementById("winter_seeds")) {
    var scene = "winter_seeds";
    var Object = React.lazy(() => import("../Seed.js"));
} else if (document.getElementById("summer_seeds")) {
    var scene = "summer_seeds";
    var Object = React.lazy(() => import("../Seed.js"));
} else if (document.getElementById("fall_seeds")) {
    var scene = "fall_seeds";
    var Object = React.lazy(() => import("../Seed.js"));
}
//Fertilizers
else if (document.getElementById("seagull_fertilizer")) {
    var scene = "seagull_fertilizer";
    var Object = React.lazy(() => import("../Fertilizer.js"));
} else if (document.getElementById("seaweed_fertilizer")) {
    var scene = "seaweed_fertilizer";
    var Object = React.lazy(() => import("../Fertilizer.js"));
}
// Boxs
else if (document.getElementById("fall_box")) {
    var scene = "fall_box";
    var Object = React.lazy(() => import("../Box_season.js"));
} else if (document.getElementById("summer_box")) {
    var scene = "summer_box";
    var Object = React.lazy(() => import("../Box_season.js"));
} else if (document.getElementById("spring_box")) {
    var scene = "spring_box";
    var Object = React.lazy(() => import("../Box_season.js"));
} else if (document.getElementById("winter_box")) {
    var scene = "winter_box";
    var Object = React.lazy(() => import("../Box_season.js"));
} else {
    console.log("oui");
}
// Loader for the scene
function Loader() {
    const { progress } = useProgress();
    return <Html center>{progress} % loaded</Html>;
}
function CameraHelper() {
    const camera = new PerspectiveCamera(60, 1, 1, 3);
    return (
        <group position={[5, 5, 15]}>
            <cameraHelper args={[camera]} />
        </group>
    );
}
// Function that generate the scene
function App() {
    return (
        <div className="scene" style={{ width: "40vw", height: "60vh" }}>
            <Canvas
                shadows
                dpr={[1, 2]}
                camera={{ position: [8, 8, 8], fov: 50 }}
            >
                <ambientLight />
                <Suspense fallback={<Loader />}>
                    <Stage environment="city" intensity={0.5}>
                        <Object />
                        {/* <CameraHelper /> */}
                    </Stage>
                    <BakeShadows />
                </Suspense>
                <OrbitControls autoRotate />
            </Canvas>
        </div>
    );
}
// Export my fonction into a DOM element
export default App;

try {
    const container = document.getElementById(scene);
    const root = createRoot(container);
    root.render(<App />);
} catch (error) {}
