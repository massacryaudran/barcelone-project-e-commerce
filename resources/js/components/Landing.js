import * as THREE from "three";
import { useEffect, useState } from "react";
import { Canvas, useFrame, useThree } from "@react-three/fiber";
import {
    Physics,
    usePlane,
    useCompoundBody,
    useSphere,
    useBox,
} from "@react-three/cannon";
import {
    Environment,
    PerformanceMonitor,
    useGLTF,
    AdaptiveDpr,
} from "@react-three/drei";
import { EffectComposer, SSAO } from "@react-three/postprocessing";

import niceColors from "nice-color-palettes";

const baubleMaterial = new THREE.MeshLambertMaterial({
    color: "#c0a090",
    emissive: "red",
});
const capMaterial = new THREE.MeshStandardMaterial({
    metalness: 1,
    roughness: 0.15,
    color: "#8a300f",
    emissive: "#600000",
    envMapIntensity: 9,
});
const sphereGeometry = new THREE.SphereGeometry(1, 28, 28);
const baubles = [...Array(7)].map(() => ({
    args: [0.6, 0.6, 0.8, 0.8, 1][Math.floor(Math.random() * 3)],
    position: [0, -0.5, 0],
    mass: 1,
    angularDamping: 0.2,
    linearDamping: 0.95,
}));
const baublesd = [...Array(7)].map(() => ({
    args: [0.6, 0.6, 0.8, 0.8, 1][Math.floor(Math.random() * 3)],
    position: [0, 0.5, 0],
    mass: 1,
    angularDamping: 0.2,
    linearDamping: 0.95,
}));
const block = [...Array(1)].map(() => ({
    args: [0.6, 0.6, 0.8, 0.8, 1][Math.floor(Math.random() * 3)],
    mass: 1,
    angularDamping: 0.2,
    linearDamping: 0.95,
}));
function Cube(props) {
    const [ref] = useBox(() => ({
        mass: 1,
        position: [0.1, 0.3, 2],
        emissive: "red",
        ...props,
    }));
    return (
        <mesh castShadow ref={ref}>
            <boxGeometry args={[2.5, 1, 8]} />
            <meshStandardMaterial color="blue" />
        </mesh>
    );
}
// Test size
let scale, args1, args2, args3, cube;
function readSize() {
    const width = window.innerWidth;
    const height = window.innerHeight;
    console.log(width);
    console.log(height);

    if (width < 1080 || height < 900) {
        scale = 0.1;
        args1 = 0.2;
        args2 = 0.8;
        args3 = 10;
        cube = 2.5;
    } else {
        cube = 5.6;
        scale = 0.18;
        args1 = 0.6;
        args2 = 1;
        args3 = 35;
    }
}
readSize();
window.addEventListener("resize", readSize);

function Flower({ vec = new THREE.Vector3(), palette, ...props }) {
    const { nodes, materials } = useGLTF(
        "/storage/objects/landing/alien-flower.glb"
    );
    const [ref, api] = useCompoundBody(() => ({
        ...props,
        shapes: [
            {
                type: "Box",
                position: [0, 0, 1.2 * props.args],
                args: new THREE.Vector3()
                    .setScalar(props.args * args1)
                    .toArray(),
            },
            { type: "Sphere", args: [props.args * args2] },
        ],
    }));
    useEffect(
        () =>
            api.position.subscribe((p) =>
                api.applyForce(
                    vec
                        .set(...p)
                        .normalize()
                        .multiplyScalar(-props.args * args3)
                        .toArray(),
                    [0, 0, 0]
                )
            ),
        [api]
    ); // prettier-ignore
    const [petalMaterial] = useState(() => {
        const color = niceColors[palette][Math.floor(Math.random() * 5)];
        const material = new THREE.MeshLambertMaterial({
            color,
            side: THREE.DoubleSide,
        });
        material.color.set(color).convertSRGBToLinear();
        return material;
    });
    return (
        <group ref={ref} dispose={null}>
            <group rotation={[0.44, -0.61, 0.66]} scale={scale}>
                <mesh
                    castShadow
                    receiveShadow
                    geometry={nodes.pPlane12_lambert5_0.geometry}
                    material={petalMaterial}
                />
                <mesh
                    castShadow
                    receiveShadow
                    geometry={nodes.pPlane12_lambert5_0_1.geometry}
                    material={materials.pistil}
                />
            </group>
        </group>
    );
}

useGLTF.preload("/storage/objects/landing/alien-flower.glb");
// Collisions that interact with the flowers, plane for the deadline of the scene and the box for the titles of the page
function Collisions() {
    const viewport = useThree((state) => state.viewport);
    usePlane(() => ({ position: [0, 0, 0], rotation: [0, 0, 0] }));
    usePlane(() => ({ position: [0, 0, 9], rotation: [0, -Math.PI, 0] }));
    usePlane(() => ({ position: [0, -5, 0], rotation: [-Math.PI / 2, 0, 0] }));
    usePlane(() => ({ position: [0, 5, 0], rotation: [Math.PI / 2, 0, 0] }));
    useBox(() => ({ position: [0.1, 0.3, 2], args: [cube, 1, 8] }));
    const [, api] = useSphere(() => ({ type: "Kinematic", args: [2] }));
    return useFrame((state) =>
        api.position.set(
            (state.mouse.x * viewport.width) / 2,
            (state.mouse.y * viewport.height) / 2,
            2.5
        )
    );
}

export const App = () => {
    const [palette] = useState(() => Math.floor(Math.random() * 100));
    const [dpr, setDpr] = useState(0.5);

    return (
        <Canvas
            shadows
            dpr={dpr}
            linear={false}
            gl={{ alpha: true, stencil: false, depth: false, antialias: false }}
            camera={{ position: [0, 0, 20], fov: 35, near: 10, far: 40 }}
            onCreated={(state) => (state.gl.toneMappingExposure = 1)}
            pixelratio={[1]}
            frameloop="demand"
        >
            <PerformanceMonitor
                onIncline={() => setDpr(0.8)}
                onDecline={() => setDpr(0.6)}
            />

            <ambientLight intensity={0.75} />
            <spotLight
                position={[20, 20, 25]}
                penumbra={1}
                angle={0.2}
                color="white"
                castShadow
                shadow-mapSize={[512, 512]}
            />
            <directionalLight position={[0, 5, -4]} intensity={4} />
            <directionalLight
                position={[0, -15, -0]}
                intensity={4}
                color="red"
            />
            <Physics gravity={[0, 0, 0]} iterations={8} broadphase="SAP">
                <Collisions />
                {
                    baubles.map((props, i) => <Flower key={i} palette={palette} {...props} />) /* prettier-ignore */
                }
                <Collisions />
                {
                    baublesd.map((props, i) => <Flower key={i} palette={palette} {...props} />) /* prettier-ignore */
                }
            </Physics>
            {/* <Physics gravity={[0, 0, 0]}>
                <Cube />
            </Physics> */}
            <Environment files="/storage/objects/landing/adamsbridge.hdr" />
            <EffectComposer multisampling={0}>
                <SSAO
                    samples={5}
                    radius={25}
                    intensity={10}
                    luminanceInfluence={0.5}
                    color="red"
                />
                <SSAO
                    samples={12}
                    radius={3}
                    intensity={15}
                    luminanceInfluence={0.5}
                    color="red"
                />
            </EffectComposer>
            <AdaptiveDpr pixelated />
            <PerformanceMonitor />
        </Canvas>
    );
};
