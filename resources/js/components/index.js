import React from "react";
import { Suspense, useReducer } from "react";
import { App } from "./Landing.js";
import { createRoot } from "react-dom/client";

if (document.getElementById("root")) {
    var page = "root";
}
function RootApp() {
    const [i, inc] = useReducer((x) => x + 1, 0);

    return (
        <>
            {/* <Underlay /> */}

            <Suspense fallback={null}>
                <App key={i} />
            </Suspense>
            <div className="container_main">
                {/* Container principal */}
                <div
                    className="container1"
                    style={{
                        position: "absolute",
                        zIndex: 1,
                        top: "47%",
                        left: "50%",
                        transform: "translate(-50% , -50%)",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        gap: "10px",
                    }}
                >
                    <img
                        id="arrowPrev"
                        src="/storage/images/left.svg"
                        alt="left arrow"
                        style={{
                            marginTop: "5px",
                            width: "40px",
                            height: "80px",
                            filter: "drop-shadow(0px 0px 0px transparent)",
                            transition: "1s filter 1s ease-in-out ",
                            transition: ".3s filter ease-in-out ",
                        }}
                    ></img>
                    <h2
                        id="title"
                        style={{
                            margin: "0",
                            color: "black !important",
                            fontSize: "3em",
                            fontFamily: "Antonio, sans-serif !important",
                            textShadow: "1px 1px 1px #FFffff",
                            filter: "drop-shadow(1px 1px 20px #ffffff)",
                            textAlign: "center",
                        }}
                    >
                        Sustainable gardening
                    </h2>

                    <img
                        id="arrowNext"
                        src="/storage/images/right.svg"
                        alt="right arrow"
                        style={{
                            marginTop: "5px",
                            filter: "drop-shadow(0px 0px 0px transparent)",
                            width: "40px",
                            height: "80px",
                            transition: ".3s filter ease-in-out ",
                        }}
                    ></img>
                </div>
                {/* Div 1 de gauche */}

                <div
                    className="sectionInSlide section"
                    id="slider0"
                    style={{
                        position: "absolute",
                        zIndex: 2,
                        top: "70%",
                        left: "20%",
                        height: "100px",
                        width: "300px",
                    }}
                >
                    <h2
                        style={{
                            fontSize: "2em",
                            margin: "0",
                        }}
                    >
                        01
                    </h2>
                    <span
                        style={{
                            fontSize: "1.8em",
                        }}
                    >
                        Our products
                    </span>
                    <div
                        style={{
                            zIndex: 2,
                            width: "150px",
                            height: "2px",
                            background: "black",
                            margin: "5px 0px 5px 0px",
                        }}
                    ></div>

                    <p>Discover all our products to grow your own plants</p>
                    <div
                        style={{
                            fontDecoration: "underline",
                            fontWeight: "bold",
                        }}
                    >
                        <a href="/category">See our products</a>
                    </div>
                    <div
                        className="triangle"
                        style={{
                            width: "0",
                            height: "0",
                            borderStyle: "solid",
                            borderWidth: "130px 130px 0 0",
                            borderColor:
                                "#ffff transparent transparent transparent",
                            position: "absolute",
                            zIndex: -1,
                            top: "-5%",
                            left: "-5%",
                        }}
                    ></div>
                </div>

                {/* Div 2 de gauche */}
                <div
                    className="sectionInSlide section stockRight"
                    id="slider1"
                    style={{
                        position: "absolute",
                        zIndex: 1,
                        top: "70%",
                        left: "20%",
                        height: "100px",
                        width: "300px",
                    }}
                >
                    <h2
                        style={{
                            fontSize: "2em",
                            margin: "0",
                        }}
                    >
                        02
                    </h2>
                    <span
                        style={{
                            fontSize: "1.8em",
                        }}
                    >
                        Our pre-made boxes
                    </span>
                    <div
                        style={{
                            zIndex: 1,
                            width: "150px",
                            height: "2px",
                            background: "black",
                            margin: "5px 0px 5px 0px",
                        }}
                    ></div>

                    <p>
                        Discover our pre-cut boxes with all the tools you need
                    </p>
                    <div
                        style={{
                            fontDecoration: "underline",
                            fontWeight: "bold",
                        }}
                    >
                        <label
                            style={{ cursor: "pointer" }}
                            htmlFor="seasonnal_boxs"
                        >
                            See our boxes
                        </label>
                    </div>
                    <div
                        className="triangle"
                        style={{
                            width: "0",
                            height: "0",
                            borderStyle: "solid",
                            borderWidth: "130px 130px 0 0",
                            borderColor:
                                "#ffff transparent transparent transparent",
                            position: "absolute",
                            zIndex: -1,
                            top: "-5%",
                            left: "-5%",
                        }}
                    ></div>
                </div>
                {/* Div 3 de gauche */}
                <div
                    className="sectionInSlide section stockRight"
                    id="slider2"
                    style={{
                        position: "absolute",
                        zIndex: 1,
                        top: "70%",
                        left: "20%",
                        height: "100px",
                        width: "300px",
                    }}
                >
                    <h2
                        style={{
                            fontSize: "2em",
                            margin: "0",
                        }}
                    >
                        03
                    </h2>
                    <span
                        style={{
                            fontSize: "1.8em",
                        }}
                    >
                        Step by step
                    </span>
                    <div
                        style={{
                            zIndex: 1,
                            width: "150px",
                            height: "2px",
                            background: "black",
                            margin: "5px 0px 5px 0px",
                        }}
                    ></div>

                    <p>
                        Follow our tutorial to learn all about how to plant a
                        seed
                    </p>
                    <div
                        style={{
                            fontDecoration: "underline",
                            fontWeight: "bold",
                        }}
                    >
                        <a href="/tutorial">Learn with us</a>
                    </div>
                    <div
                        className="triangle"
                        style={{
                            width: "0",
                            height: "0",
                            borderStyle: "solid",
                            borderWidth: "130px 130px 0 0",
                            borderColor:
                                "#ffff transparent transparent transparent",
                            position: "absolute",
                            zIndex: -1,
                            top: "-5%",
                            left: "-5%",
                        }}
                    ></div>
                </div>

                {/* Div 1 de droite*/}
                <div
                    className="section"
                    id="slidar0"
                    style={{
                        position: "absolute",
                        zIndex: 2,
                        top: "70%",
                        right: "20%",
                        height: "fit-content",
                        width: "300px",
                    }}
                >
                    <h2
                        className="title-section"
                        style={{
                            fontSize: "2em",
                            margin: "0",
                        }}
                    >
                        Sustainable
                    </h2>
                    <span
                        style={{
                            fontSize: "1.8em",
                        }}
                    >
                        Materials
                    </span>

                    <div
                        style={{
                            zIndex: 2,
                            width: "150px",
                            height: "2px",
                            background: "black",
                            margin: "5px 0px 5px 0px",
                        }}
                    ></div>
                    <p>Made with sustainable materials from the sea</p>
                    <div
                        style={{
                            fontDecoration: "underline",
                            fontWeight: "bold",
                        }}
                    >
                        <a href="/about_us">About us</a>
                    </div>
                    <div
                        className="triangledown"
                        style={{
                            width: "0",
                            height: "0",
                            borderStyle: "solid",
                            borderWidth: "0 0 130px 130px",
                            borderColor:
                                "transparent transparent #ffff transparent",
                            position: "absolute",
                            zIndex: -1,
                            bottom: "0%",
                            right: "0%",
                        }}
                    ></div>
                </div>
                {/* Div 2 de droite */}
                <div
                    id="slidar1"
                    className="section stockRight"
                    style={{
                        position: "absolute",
                        zIndex: 1,
                        top: "70%",
                        right: "20%",
                        height: "fit-content",
                        width: "300px",
                    }}
                >
                    <h2
                        style={{
                            fontSize: "2em",
                            margin: "0",
                        }}
                    >
                        Your box
                    </h2>
                    <span
                        style={{
                            fontSize: "1.8em",
                        }}
                    >
                        Personalized gift
                    </span>
                    <div
                        style={{
                            zIndex: 1,
                            width: "150px",
                            height: "2px",
                            background: "black",
                            margin: "5px 0px 5px 0px",
                        }}
                    ></div>
                    <p>
                        Personalized according to your desires (tools, seeds,
                        pots)
                    </p>
                    <div
                        style={{
                            fontDecoration: "underline",
                            fontWeight: "bold",
                        }}
                    >
                        <a href="/make_my_box">Personalize it</a>
                    </div>
                    <div
                        className="triangledown"
                        style={{
                            width: "0",
                            height: "0",
                            borderStyle: "solid",
                            borderWidth: "0 0 130px 130px",
                            borderColor:
                                "transparent transparent #ffff transparent",
                            position: "absolute",
                            zIndex: -1,
                            bottom: "0%",
                            right: "0%",
                        }}
                    ></div>
                </div>
                {/* Div 3 de droite */}
                <div
                    id="slidar2"
                    className="section stockRight"
                    style={{
                        position: "absolute",
                        zIndex: 2,
                        top: "70%",
                        right: "20%",
                        height: "fit-content",
                        width: "300px",
                    }}
                >
                    <h2
                        style={{
                            fontSize: "2em",
                            margin: "0",
                        }}
                    >
                        Informative
                    </h2>
                    <span
                        style={{
                            fontSize: "1.8em",
                        }}
                    >
                        Find your needs
                    </span>
                    <div
                        style={{
                            zIndex: 2,
                            width: "150px",
                            height: "2px",
                            background: "black",
                            margin: "5px 0px 5px 0px",
                        }}
                    ></div>
                    <p>
                        Find all the tools you need to take care of your plant
                    </p>
                    <div
                        style={{
                            fontDecoration: "underline",
                            fontWeight: "bold",
                        }}
                    >
                        <a href="/tutorial">Search your tools</a>
                    </div>
                    <div
                        className="triangledown"
                        style={{
                            width: "0",
                            height: "0",
                            borderStyle: "solid",
                            borderWidth: "0 0 130px 130px",
                            borderColor:
                                "transparent transparent #ffff transparent",
                            position: "absolute",
                            zIndex: -1,
                            bottom: "0%",
                            right: "0%",
                        }}
                    ></div>
                </div>
            </div>
            {/* Div du menu */}
            <div
                style={{
                    position: "absolute",
                    zIndex: 1,
                    top: "0%",
                    left: "50%",
                    transform: "translateX(-50%)",
                }}
            >
                <h1
                    className="title-brand"
                    style={{
                        color: "black",
                        fontSize: "50pt",
                        margin: "0",
                        fontFamily: "Antonio, sans-serif",
                        textTransform: "uppercase",
                        filter: "drop-shadow(3px 3px 3px #0000007f)",
                    }}
                >
                    Bunty-G
                </h1>
            </div>
            <div
                style={{
                    position: "absolute",
                    zIndex: 1,
                    bottom: "0%",
                    left: "50%",
                    transform: "translateX(-50%)",
                }}
            >
                <img
                    className="logo_landing"
                    src="/storage/images/logo.svg"
                    alt="brand logo"
                    style={{
                        width: "80px",
                        height: "80px",
                        filter: "drop-shadow(0px 0px 5px #0000007f)",
                    }}
                ></img>
            </div>

            <div
                className="container_a"
                style={{
                    position: "absolute",
                    zIndex: 1,
                    top: "47%",
                    transform: "translateY(-50%)",
                    left: "1%",
                    padding: "20px",
                    display: "flex",
                    flexDirection: "column",
                    gap: "20px",
                }}
            >
                <a href="/category" style={{ width: "fit-content" }}>
                    <button
                        className="menubtn"
                        style={{
                            border: "2px solid rgba(0,0,0,0.3)",
                            fontFamily: "Antonio, sans-serif",
                            fontSize: "20pt",
                            textTransform: "uppercase",
                            padding: "10px 20px",
                            borderRadius: "3px",
                            background: "transparent",
                        }}
                    >
                        Products
                    </button>
                </a>

                <a href="/tutorial" style={{ width: "fit-content" }}>
                    <button
                        className="menubtn"
                        style={{
                            border: "2px solid rgba(0,0,0,0.3)",
                            fontFamily: "Antonio, sans-serif",
                            fontSize: "20pt",
                            textTransform: "uppercase",
                            padding: "10px 20px",
                            borderRadius: "3px",
                            background: "transparent",
                        }}
                    >
                        How to start
                    </button>
                </a>

                <a href="/make_my_box" style={{ width: "fit-content" }}>
                    <button
                        className="menubtn"
                        style={{
                            border: "2px solid rgba(0,0,0,0.3)",
                            fontFamily: "Antonio, sans-serif",
                            fontSize: "20pt",
                            textTransform: "uppercase",
                            padding: "10px 20px",
                            borderRadius: "3px",
                            background: "transparent",
                        }}
                    >
                        Make my Box
                    </button>
                </a>

                <a href="/about_us" style={{ width: "fit-content" }}>
                    <button
                        className="menubtn"
                        style={{
                            border: "2px solid rgba(0,0,0,0.3)",
                            fontFamily: "Antonio, sans-serif",
                            fontSize: "20pt",
                            textTransform: "uppercase",
                            padding: "10px 20px",
                            borderRadius: "3px",
                            background: "transparent",
                        }}
                    >
                        About us
                    </button>
                </a>

                <a href="/support" style={{ width: "fit-content" }}>
                    <button
                        className="menubtn"
                        style={{
                            border: "2px solid rgba(0,0,0,0.3)",
                            fontFamily: "Antonio, sans-serif",
                            fontSize: "20pt",
                            textTransform: "uppercase",
                            padding: "10px 20px",
                            borderRadius: "3px",
                            background: "transparent",
                        }}
                    >
                        Support
                    </button>
                </a>
            </div>
            {/* <Overlay /> */}
        </>
    );
}

export default RootApp;

try {
    const container = document.getElementById(page);
    const root = createRoot(container);
    root.render(<RootApp />);
} catch (error) {}
