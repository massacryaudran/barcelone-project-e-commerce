import React, { Suspense, useRef, useState, useEffect } from "react";
import { Canvas } from "@react-three/fiber";
import { ContactShadows, useGLTF, OrbitControls } from "@react-three/drei";
import { CirclePicker } from "react-color";
import { proxy, useSnapshot } from "valtio";
import { createRoot } from "react-dom/client";
import { Physics, useBox } from "@react-three/cannon";
import * as THREE from "three";

if (document.getElementById("boxmakerstep")) {
    var boxmaker = "boxmakerstep";
    var Plane = React.lazy(() => import("../Plane.js"));
    var Flower = React.lazy(() => import("../Flower.js"));
    var Flower2 = React.lazy(() => import("../Flower2.js"));
}
// Using a Valtio state model to bridge reactivity between
// the canvas and the dom, both can write to it and/or react to it.
const state = proxy({
    colors: {
        selectedPaint: { hex: "#ffffff", name: "Blanc" },
        selectedWood: { hex: "#baa48a", name: "Chêne" },
        stainlessSteel: { hex: "#cccccc", name: "Inox" },
    },
});
// Objct cloné

/* Geometry */
const Planed = () => (
    <mesh rotation={[-Math.PI / 2, 0, 0]} position={[0, -0.5, 0]}>
        <planeBufferGeometry attach="geometry" args={[100, 100]} />
        <meshPhysicalMaterial attach="material" color="white" />
    </mesh>
);
function Box(props) {
    const ref = useRef();
    const snap = useSnapshot(state);
    const { nodes, materials } = useGLTF(
        "/storage/objects/box_maker/box/box.gltf"
    );

    return (
        <group {...props} dispose={null}>
            <mesh
                geometry={nodes.Box_Base.geometry}
                material={materials["Material.003"]}
                material-color={snap.colors.selectedPaint.hex}
                position={[-1.99, 0.23, -0.57]}
                rotation={[Math.PI / 2, 0, 0]}
                scale={0.04}
            >
                <mesh
                    geometry={nodes.Box_faces.geometry}
                    material={materials.Box_faces}
                    material-color={snap.colors.selectedWood.hex}
                />
                <mesh
                    geometry={nodes.Text.geometry}
                    material={materials["Material.001"]}
                    position={[69.98, 38.15, -12.13]}
                    rotation={[0, 0, -0.06]}
                    scale={[5.96, 5.34, 5.33]}
                />
            </mesh>
            <mesh
                geometry={nodes.Box_Base001.geometry}
                material={materials["Material.006"]}
                position={[-1.99, 0.23, -0.57]}
                rotation={[Math.PI / 2, 0, 0]}
                scale={0.04}
            />
        </group>
    );
}
useGLTF.preload("/storage/objects/box_maker/box/box.gltf");

function Seeds({ vec = new THREE.Vector3(), ...props }) {
    const { nodes, materials } = useGLTF("/storage/objects/seeds/scene.gltf");
    const [ref, api] = useBox(() => ({
        mass: 5,
        shapes: [
            {
                type: "Box",
                position: [0, 0, 1.2 * props.args],
                args: new THREE.Vector3().setScalar(props.args * 0.6).toArray(),
            },
            { type: "Sphere", args: [props.args] },
        ],
    }));
    useEffect(() => api.position.subscribe((p) => api.applyForce(vec.set(...p).normalize().multiplyScalar(-props.args * 35).toArray(), [0, 0, 0])), [api]) // prettier-ignore
    return (
        <group {...props} dispose={null}>
            <group rotation={[-Math.PI / 2, 0, 0]} position={(0, -100, 0)}>
                <group rotation={[Math.PI / 2, 0, 0]} scale={0.02}>
                    <mesh
                        geometry={
                            nodes
                                .Paper_Takeaway_Bag_v1_001_Paper_Takeaway_Bag_v1_001_Tex_0
                                .geometry
                        }
                        material={materials.Paper_Takeaway_Bag_v1_001_Tex}
                    />
                </group>
            </group>
        </group>
    );
}
useGLTF.preload("/storage/objects/seeds/scene.gltf");

function Books({ vec = new THREE.Vector3(), ...props }) {
    const { nodes, materials } = useGLTF("/storage/objects/books/scene.gltf");
    const [ref, api] = useBox(() => ({
        mass: 5,
        shapes: [
            {
                type: "Box",
                position: [0, 0, 1.2 * props.args],
                args: new THREE.Vector3().setScalar(props.args * 0.6).toArray(),
            },
            { type: "Sphere", args: [props.args] },
        ],
    }));
    useEffect(() => api.position.subscribe((p) => api.applyForce(vec.set(...p).normalize().multiplyScalar(-props.args * 35).toArray(), [0, 0, 0])), [api]) // prettier-ignore
    return (
        <group {...props} dispose={null}>
            <group rotation={[-Math.PI / 2, 0, 0]} scale={0.01}>
                <mesh
                    geometry={nodes.Object_2.geometry}
                    material={materials.Copertina}
                />
                <mesh
                    geometry={nodes.Object_3.geometry}
                    material={materials.Pagine_Centrali}
                />
                <mesh
                    geometry={nodes.Object_4.geometry}
                    material={materials.Pagine_Laterali}
                />
            </group>
        </group>
    );
}
useGLTF.preload("/storage/objects/books/scene.gltf");

export default function App() {
    const snap = useSnapshot(state);
    const paints = [
        { hex: "#ffffff", name: "White" },
        { hex: "#abdee6", name: "Blue" },
        { hex: "#cbaacb", name: "Purple" },
        { hex: "#ffffa1", name: "Yellow" },
        { hex: "#ffccb6", name: "Orange" },
        { hex: "#f3b0c3", name: "Pink" },
    ];
    const woodSpecies = [
        { hex: "#a5914c", name: "Oak" },
        { hex: "#efe1a2", name: "Ash" },
        { hex: "#776d4a", name: "Oliver" },
    ];
    const seeds = [{ hex: "#2a0116", hex: "#2a0116", hex: "#2a0116" }];
    const [x, setX] = useState(-10);
    const [xx, setX1] = useState(-10);
    return (
        <div>
            <div className="wrapper">
                <div
                    className="content w-embed container"
                    id="box-3D"
                    style={{ width: "70vw", height: "50vh" }}
                >
                    <Canvas
                        onPointerOver={() => console.log("PointerOver")} // the mouse has entered the canvas
                        onPointerOut={() => console.log("onPointerOut")} // the mouse has left the canvas
                        // onPointerMissed={() => console.log("onPointerMissed")} // ???
                        onPointerDown={() => console.log("onPointerDown")} // the canvas has been clicked
                        className="content w-embed"
                        pixelratio={[1, 2]}
                        // camera={{
                        // position: [0, 0, 20],
                        // fov: 80,
                        // aspect: window.innerWidth / window.innerHeight,
                        // near: 1,
                        // far: 10000,
                        // lookAt ?
                        //}}
                    >
                        <ambientLight intensity={2} />
                        <spotLight
                            intensity={0}
                            angle={1}
                            penumbra={1}
                            position={[5, 25, 20]}
                        />
                        <Suspense fallback={null}>
                            <Physics>
                                <Box scale={0.8} position={[-0.1, 0, 0.2]} />
                                <Flower
                                    scale={0.028}
                                    position={[-4, 0.07, 0]}
                                />
                                <Flower2
                                    scale={0.028}
                                    position={[4, 0.07, 0]}
                                />
                                <Plane scale={0.6} position={[0, 0, 0]} />

                                <Seeds scale={6} position={[0.5, x, 0]} />
                                <Books
                                    scale={0.5}
                                    position={[0, xx, 0]}
                                    rotation={[Math.PI / 2, 1.58, 0]}
                                />
                            </Physics>
                            <ContactShadows
                                rotation-x={Math.PI / 2}
                                position={[0, -0.35, 0]}
                                opacity={0.25}
                                width={10}
                                height={10}
                                blur={2}
                                far={1}
                            />
                        </Suspense>
                        <OrbitControls
                            // How far you can orbit vertically, upper and lower limits.
                            // Range is 0 to Math.PI radians.
                            minPolarAngle={Math.PI / 3}
                            maxPolarAngle={Math.PI / 2.5}
                            // How far you can orbit horizontally, upper and lower limits.
                            // If set, the interval [ min, max ] must be a sub-interval of [ - 2 PI, 2 PI ], with ( max - min < 2 PI )
                            minAzimuthAngle={-Infinity}
                            maxAzimuthAngle={Infinity}
                            maxDistance={8}
                            minDistance={0.5}
                            enableZoom={true}
                            enablePan={false}
                            autoRotate={true}
                            autoRotateSpeed={1}
                            // only on the grid page with all products?
                        />
                    </Canvas>
                </div>

                <div className="sidebar w-embed"
                    id="texture-choice"
                >
                    <p>Now it is time to choose the color of your box ! </p>
                    <div
                        className="texture-display"
                        style={{
                            display: "flex",
                            flexdirection: "column",
                            justifyContent: "space-between",
                        }}
                    >
                        <div style={{ alignItems: "center" }}>
                            <h3 style={{}}>Coloris</h3>
                            <CirclePicker
                                colors={paints.map((x) => x.hex)} // {["#fff", ...]}
                                color={snap.colors.selectedPaint.hex}
                                /*
              onChange={(color) => {
                state.colors.selectedPaint = {
                  hex: color.hex,
                  name: "test",
                }
              }}
              */
                                onChange={(color) => {
                                    paints.find((paint) => {
                                        if (paint.hex === color.hex) {
                                            state.colors.selectedPaint = {
                                                hex: color.hex,
                                                name: paint.name,
                                            };
                                        }
                                    });
                                }}
                            />
                        </div>
                        <h4
                            className="color-display"
                            style={{
                                fontWeight: "300",
                                fontStyle: "italic",
                            }}
                        >
                            ({state.colors.selectedPaint.name})
                        </h4>
                        <div 
                            className="color-display-right"
                            style={{ alignItems: "center" }}>
                            <h3 style={{}}>Texture</h3>
                            <CirclePicker
                                colors={woodSpecies.map((x) => x.hex)} // {["#baa48a", ...]}
                                color={snap.colors.selectedWood.hex}
                                // onChange={(color) => (state.colors.selectedWood = { hex: color.hex, name: "retest" })}
                                onChange={(color) => {
                                    woodSpecies.find((wood) => {
                                        if (wood.hex === color.hex) {
                                            state.colors.selectedWood = {
                                                hex: color.hex,
                                                name: wood.name,
                                            };
                                        }
                                    });
                                }}
                            />
                        </div>

                        <h4
                            className="color-display"
                            style={{
                                fontWeight: "300",
                                fontStyle: "italic",
                            }}
                        >
                            ({snap.colors.selectedWood.name})
                        </h4>
                    </div>
                    <div
                        style={{
                            display: "flex",
                            justifyContent: "space-between",
                        }}
                    >
                        <div
                            style={{
                                display: "flex",
                                justifyContent: "flex-start",
                            }}
                        >
                            <p
                                style={{
                                    fontWeight: "700",
                                    marginRight: ".5rem",
                                }}
                            >
                                120 €
                            </p>
                            <p
                                style={{
                                    fontWeight: "400",
                                    textDecoration: "line-through",
                                }}
                            >
                                150 €
                            </p>
                        </div>
                    </div>
                    <div
                        className="addon"
                        style={{
                            display: "flex",
                            flex: "1",
                            flexDirection: "row",
                        }}
                    >
                        <div className="seeds">
                            <p>Do you want a bag of seed ? Check if yes !</p>

                            <CirclePicker
                                colors={seeds.map((x) => x.hex)}
                                // onChange={(color) => (state.colors.selectedWood = { hex: color.hex, name: "retest" })}
                                onChange={() => {
                                    setX(1.1);
                                }}
                            />
                        </div>

                        <div className="books">
                            <p>Do you want a book ? Check if yes !</p>

                            <CirclePicker
                                colors={seeds.map((x1) => x1.hex)}
                                // onChange={(color) => (state.colors.selectedWood = { hex: color.hex, name: "retest" })}
                                onChange={() => {
                                    setX1(1.5);
                                }}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

try {
    const container = document.getElementById(boxmaker);
    const root = createRoot(container);

    root.render(<App />);
} catch (error) {}
