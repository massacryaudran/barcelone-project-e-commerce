import React, { useRef } from "react";
import { useGLTF } from "@react-three/drei";

export default function Transplanter(props) {
    const { nodes, materials } = useGLTF(
        "/storage/objects/tools/transplanter/transplanter.gltf"
    );
    return (
        <group {...props} dispose={null}>
            <group position={[-3.45, 2.62, -10.73]}>
                <mesh
                    geometry={nodes.Plane009.geometry}
                    material={materials.garden_tools}
                />
                <mesh
                    geometry={nodes.Plane009_1.geometry}
                    material={materials.Material}
                />
            </group>
        </group>
    );
}

useGLTF.preload("/storage/objects/tools/transplanter/transplanter.gltf");
