import React, { useRef } from "react";
import { useGLTF } from "@react-three/drei";

export default function Wateringcan(props) {
    const { nodes, materials } = useGLTF(
        "/storage/objects/tools/wateringcan/wateringcan.gltf"
    );
    return (
        <group {...props} dispose={null}>
            <mesh
                geometry={nodes.AO1_WateringCan_fixed.geometry}
                material={materials["White rice paper texture"]}
                rotation={[Math.PI / 2, 0, 0]}
                position={[0, -50, 0]}
                scale={[1, 1, 1]}
            />
        </group>
    );
}

useGLTF.preload("/storage/objects/tools/wateringcan/wateringcan.gltf");
