import React, { useRef } from "react";
import { useGLTF } from "@react-three/drei";

export default function Model(props) {
    const { nodes, materials } = useGLTF("/storage/objects/books/scene.gltf");
    return (
        <group {...props} dispose={null}>
            <group scale={0.01}>
                <mesh
                    geometry={nodes.Object_2.geometry}
                    material={materials.Copertina}
                />
                <mesh
                    geometry={nodes.Object_3.geometry}
                    material={materials.Pagine_Centrali}
                />
                <mesh
                    geometry={nodes.Object_4.geometry}
                    material={materials.Pagine_Laterali}
                />
            </group>
        </group>
    );
}

useGLTF.preload("/storage/objects/books/scene.gltf");
