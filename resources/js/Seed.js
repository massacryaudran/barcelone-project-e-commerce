import React, { useRef } from "react";
import { useGLTF } from "@react-three/drei";

export default function Model(props) {
    const { nodes, materials } = useGLTF("/storage/objects/seeds/scene.gltf");
    return (
        <group {...props} dispose={null}>
            <group rotation={[-Math.PI / 2, 0, 0]} position={(0, -100, 0)}>
                <group rotation={[Math.PI / 2, 0, 0]} scale={0.02}>
                    <mesh
                        geometry={
                            nodes
                                .Paper_Takeaway_Bag_v1_001_Paper_Takeaway_Bag_v1_001_Tex_0
                                .geometry
                        }
                        material={materials.Paper_Takeaway_Bag_v1_001_Tex}
                    />
                </group>
            </group>
        </group>
    );
}

useGLTF.preload("/storage/objects/seeds/scene.gltf");
