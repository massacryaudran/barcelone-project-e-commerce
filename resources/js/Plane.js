import React, { useRef } from "react";
import { useGLTF } from "@react-three/drei";

export default function Model(props) {
    const { nodes, materials } = useGLTF(
        "/storage/objects//box_maker/plane/plane.gltf"
    );
    return (
        <group {...props} dispose={null}>
            <mesh
                geometry={nodes.beech1006.geometry}
                material={materials.gradient_texture}
                position={[-0.86, 0.22, 3.02]}
                rotation={[0, -1.07, 0]}
                scale={0.08}
            >
                <mesh
                    geometry={nodes.Cylinder.geometry}
                    material={materials.Material}
                    position={[-26.98, -1.51, -26.27]}
                    rotation={[0, 1.07, 0]}
                    scale={[97.34, 1.59, 97.99]}
                />
                <mesh
                    geometry={nodes.Plane026_1.geometry}
                    material={materials.gradient_texture}
                    position={[-22.74, -0.16, -24.15]}
                    rotation={[0, 1.07, 0]}
                    scale={32.07}
                />
                <group
                    position={[-95.1, -0.79, -23.67]}
                    rotation={[Math.PI / 2, 0, -1.51]}
                    scale={0.72}
                >
                    <mesh
                        geometry={nodes.tree004_1.geometry}
                        material={materials["flower daffodil.001"]}
                    />
                    <mesh
                        geometry={nodes.tree004_2.geometry}
                        material={materials["leaf bush.001"]}
                    />
                    <mesh
                        geometry={nodes.tree004_3.geometry}
                        material={materials["plant01.001"]}
                    />
                </group>
                <group
                    position={[5.67, -0.79, 31.6]}
                    rotation={[Math.PI / 2, 0, -2.06]}
                    scale={0.72}
                >
                    <mesh
                        geometry={nodes.tree005_1.geometry}
                        material={materials["flower daffodil.002"]}
                    />
                    <mesh
                        geometry={nodes.tree005_2.geometry}
                        material={materials["leaf bush.002"]}
                    />
                    <mesh
                        geometry={nodes.tree005_3.geometry}
                        material={materials["plant01.002"]}
                    />
                </group>
                <group
                    position={[-54.38, -0.79, -87.95]}
                    rotation={[Math.PI / 2, 0, -1.51]}
                    scale={0.72}
                >
                    <mesh
                        geometry={nodes.tree003_1.geometry}
                        material={materials["flower daffodil.009"]}
                    />
                    <mesh
                        geometry={nodes.tree003_2.geometry}
                        material={materials["leaf bush.009"]}
                    />
                    <mesh
                        geometry={nodes.tree003_3.geometry}
                        material={materials["plant01.009"]}
                    />
                </group>
                <group
                    position={[39.13, -0.79, -29.41]}
                    rotation={[Math.PI / 2, 0, -1.72]}
                    scale={0.72}
                >
                    <mesh
                        geometry={nodes.tree006_1.geometry}
                        material={materials["flower daffodil.003"]}
                    />
                    <mesh
                        geometry={nodes.tree006_2.geometry}
                        material={materials["leaf bush.003"]}
                    />
                    <mesh
                        geometry={nodes.tree006_3.geometry}
                        material={materials["plant01.003"]}
                    />
                </group>
                <group
                    position={[-64.64, -0.79, 29.16]}
                    rotation={[Math.PI / 2, 0, -1.51]}
                    scale={0.72}
                >
                    <mesh
                        geometry={nodes.tree007.geometry}
                        material={materials["flower daffodil.004"]}
                    />
                    <mesh
                        geometry={nodes.tree007_1.geometry}
                        material={materials["leaf bush.004"]}
                    />
                    <mesh
                        geometry={nodes.tree007_2.geometry}
                        material={materials["plant01.004"]}
                    />
                </group>
                <group
                    position={[8.33, -0.79, -84.06]}
                    rotation={[Math.PI / 2, 0, -1.51]}
                    scale={0.72}
                >
                    <mesh
                        geometry={nodes.tree008.geometry}
                        material={materials["flower daffodil.005"]}
                    />
                    <mesh
                        geometry={nodes.tree008_1.geometry}
                        material={materials["leaf bush.005"]}
                    />
                    <mesh
                        geometry={nodes.tree008_2.geometry}
                        material={materials["plant01.005"]}
                    />
                </group>
            </mesh>
        </group>
    );
}

useGLTF.preload("/storage/objects//box_maker/plane/plane.gltf");
