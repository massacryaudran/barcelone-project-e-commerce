import React, { useRef, useEffect } from "react";
import { useGLTF, useAnimations } from "@react-three/drei";

export default function Model(props) {
    const group = useRef();
    const { nodes, materials, animations } = useGLTF(
        "/storage/objects/box_maker/flowers/flower.gltf"
    );
    const { actions, names } = useAnimations(animations, group);
    useEffect(() => {
        // Reset and fade in animation after an index has been changed
        actions[names].reset().fadeIn(0.5).play();
        // In the clean-up phase, fade it out
        return () => actions[names].fadeOut(0.5);
    }, [actions, names]);

    return (
        <group ref={group} {...props} dispose={null}>
            <group name="Scene">
                <group
                    name="flower008_260"
                    position={[-0.05, 7.12, 0.37]}
                    rotation={[0, 0.21, 0]}
                    scale={10.09}
                >
                    <group
                        name="Armature008_258"
                        position={[0, -0.71, 0]}
                        rotation={[0, 1.57, 0]}
                        scale={0.1}
                    >
                        <group name="GLTF_created_8">
                            <group name="Plane017_257" />
                            <primitive
                                object={nodes.GLTF_created_8_rootJoint}
                            />
                            <skinnedMesh
                                name="Object_272"
                                geometry={nodes.Object_272.geometry}
                                material={materials.gradient_texture}
                                skeleton={nodes.Object_272.skeleton}
                            />
                        </group>
                    </group>
                    <group
                        name="Plane016_259"
                        position={[-0.07, -0.69, 0.08]}
                        rotation={[2.91, 0.38, -3.01]}
                        scale={0.47}
                    >
                        <mesh
                            name="mesh_17"
                            geometry={nodes.mesh_17.geometry}
                            material={materials.gradient_texture}
                            morphTargetDictionary={
                                nodes.mesh_17.morphTargetDictionary
                            }
                            morphTargetInfluences={
                                nodes.mesh_17.morphTargetInfluences
                            }
                        />
                    </group>
                </group>
            </group>
        </group>
    );
}

useGLTF.preload("/storage/objects/box_maker/flowers/flower.gltf");
