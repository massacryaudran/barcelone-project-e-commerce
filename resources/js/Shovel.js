import React, { useRef } from "react";
import { useGLTF } from "@react-three/drei";

export default function Shovel(props) {
    const { nodes, materials } = useGLTF(
        "/storage/objects/tools/shovel/shovel.gltf"
    );
    return (
        <group {...props} dispose={null}>
            <group position={[-2.81, 8.19, -10.73]}>
                <mesh
                    geometry={nodes.Plane005.geometry}
                    material={materials.garden_tools}
                />
                <mesh
                    geometry={nodes.Plane005_1.geometry}
                    material={materials.Material}
                />
            </group>
        </group>
    );
}

useGLTF.preload("/storage/objects/tools/shovel/shovel.gltf");
