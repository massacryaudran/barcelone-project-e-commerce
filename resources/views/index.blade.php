@extends('layouts.master')
<!DOCTYPE html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Interested in gardening? Shop with us for sustainable and eco-friendly tools! ">
<meta http-equiv="Cache-control" content="no-cache">
<meta http-equiv="Expires" content="-1">
<link rel="stylesheet" href="{{ asset('css/landing.css') }}"> 
<link rel="stylesheet" href="{{ asset('css/app.css') }}"> 

<script src="{{ mix('js/app.js') }}" defer></script>
<script src="{{ asset('js/closeAlert.js') }}" defer type="module"></script>
<title>Bunty-G - Home</title>


@if (session('success'))
  <div class="alert alert-succes">
    <button class="close">&times;</button>
    {{session('success')}} 
  </div>
  @endif
  @if (session('error'))
  <div class="alert alert-error">
    <button class="close">&times;</button>
    {{session('error')}} 
  </div>
  @endif
<main>
{{-- 3d element and menu --}}
  <div id="root"></div>
    {{-- Div account on the right --}}
    <div class="container_account_basket">    
      @if (Auth::check())     
        <a class="link" href="{{ route('account.index') }}" aria-label="account page redirection"><img class="img_link" src="{{ asset('/storage/images/girl.svg') }}" alt="account page" class="img-basket4"></a><span class="login-home">Account</span>
        @if ($cartCollection->count() > 0)
          <a href="/cart" aria-label="basket page redirection">
            <img class="img_link" src="{{ asset('/storage/images/basket2.svg') }}" alt="basket page logo">    
          </a>
        <span class="count-basket">{{ $cartCollection->count() }}</span>
          @else
            <a href="/cart" aria-label="basket page redirection">
              <img class="img_link" src="/storage/images/empty_basket.svg" alt="empty basket page logo">    
            </a>
          <span class="count-basket">{{ $cartCollection->count() }}</span>
          @endif
        @else
          <a href="/login" class="link" aria-label="login page redirection">
            <img class="img_link" src="/storage/images/girl_normal.svg" alt="login page">      
          </a>
            <span class="login-home">Login</span>
              @if ($cartCollection->count() > 0)
              <a href="/cart" aria-label="basket page redirection">
                <img class="img_link" src="{{ asset('/storage/images/basket2.svg') }}" alt="basket page logo">    
              </a>
                <span class="count-basket">{{ $cartCollection->count() }}</span>
              @else
                <a href="/cart" aria-label="basket page redirection">
                  <img class="img_link" src="/storage/images/empty_basket.svg" alt="empty basket page logo">    
                </a>
                 <span class="count-basket">{{ $cartCollection->count() }}</span>
              @endif
      @endif   
    </div>
      <form method="post" action="{{ route('category.pannel',['slugurl'=>'seasonnas_boxs']) }}">
      @csrf
        <input type="hidden" name="category_id" value="6">
          <i><button style="display:none;" id="seasonnal_boxs" type="submit"><b>Views our seasonnal boxs</b></button></i>
    </form>
	<noscript>
		You need to enable JavaScript to run this app.
	</noscript>
</body>
</html>
</main>
<script src="{{ asset('js/carossel.js') }}" defer></script> 

