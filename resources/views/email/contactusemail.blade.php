@component('mail::message')
Hello <b>{{ $mailData['firstname'] }} {{ $mailData['lastname'] }}</b>,
your message about <b>{{ $mailData['subject'] }}</b> has been successfully sent !<br>
@component('mail::panel')
As a reminder, your message was: <br>
<i> {{ $mailData['message'] }} <i><br>
@endcomponent
We will get back to you as soon as possible <br>
Thanks,<br>
{{ config('app.name') }}
@endcomponent
