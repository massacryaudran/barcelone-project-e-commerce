@extends('layouts.master')
<!DOCTYPE html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="If you encountered some problems, please let us know with the form">
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <title>Bunty-G - Support</title>
</head>
     <main>
    <div class="support-page">
    <h2>Support</h2>
    <div class="img_decoration">
      <img src="{{ asset('/storage/images/flower_support.png')}}" class="img-support support-left" alt="flower left">
    </div>
    <div class="img_decoration">
      <img src="{{ asset('/storage/images/flower_support.png')}}" class="img-support support-right" alt="flower right">
    </div>
    <p class="form-text">Any information? Delivery problems? Let us know about your problems as precisely as possible. We will answer you as soon as possible within 2 weeks maximum.</p>
    <form method="post" action="{{ route('send-mail') }}" class="form-support">
      @csrf
      <div class="first-half">
        <input type="text" class="input-support" id="name" placeholder="First name" name="firstname">
        <input type="text" class="input-support" id="lastname" placeholder="Last name" name="lastname">
        <input type="email" class="input-support" id="email" placeholder="Email address" name="email">
        <input type="text" class="input-support" id="subject" placeholder="Subject" name="subject">
      </div>
      <div class="second-half">
        <textarea name="message" type="text" class="message-support" id="message" placeholder="Message"></textarea>
        <p class="legal-mention-support">For all questions related to personal data, see our legal mentions <a class="info-mention" href="{{ route('index.mention') }}" target="_blank">?</a></p>
        <input type="submit" value="Submit" class="input-submit" id="button">
      </div>  
    </form>
  </div>
     </main>