@extends('layouts.master')
<!DOCTYPE html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Look at our products !">
  <link rel="stylesheet" href="{{ asset('css/app.css') }}"> 
  <title>Bunty-G - Products</title>
</head>
<main>
  <h2 class="title-category">{{$category->name}}</h2>
  <div class="product-arrangement">
    @foreach ($products as $product )
    <div class="product-card">
      <div class="product-image">
        <img class="img-product-card" src="{{asset('storage/' . $product->image) }}" alt="{{$product->name}}"> 
        <div class="prod-card-information">
          <p class="litle-description">{{$product->subtitle}}</p>
          <span><img class="card_dimension" src="{{ asset('/storage/images/prod_dimension.png') }}" alt="product dimenson logo"><p>{{$product->characteristic_dimension}}</p></span>
          <span><img class="card_weight" src="{{ asset('/storage/images/prod_weight.png') }}" alt="product weight logo"><p>{{$product->characteristic_weight}}</p></span>
        </div>
      </div>
      <div class="product-content">
        <div class="product-information">
          <h4 class="product-name">{{$product->name}}</h4>
          <div class="product-price" aria label="The price of the {{$product->name}} is">{{$product->getEuropePrice()}}</div>          
        </div>
        <a href="{{ route('product.show', $product->slug) }}" class="to-cart" aria-label="product page redirection"><img class="img-to-cart" src="{{ asset('/storage/images/info_icon.png') }}" alt="product page redirection logo"></a>
      </div>
    </div>
    @endforeach
  </div>
</main>

