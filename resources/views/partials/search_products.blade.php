<div class="search_container"> 
  <form action="{{ route('products.search') }}" class="product-search">
    <input type="search" name="q" class="search_bar_input" id="inputbar" value="{{ request()->q ?? ''  }}" placeholder="Search...">
      <button type="submit" class="search-button"> 
        <img src="{{ asset('/storage/images/search.svg') }}" class="search_img" id="search">
      </button>
  </form>
</div>

<style> 
    input:placeholder-shown { 
  font-style: italic;
  font-weight: 200;
}
</style>

             