@extends('layouts.master')
<!DOCTYPE html>
<head>
  <html lang="en" >
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Consult your basket">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  <script src="{{ asset('js/cart-number.js') }}" defer></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
</head>

  <title>Bunty-G - Cart</title>
   <main>
@if ($cartCollection->count() > 0)

<div class="container-basket">

  <h2>Cart page</h2>

  <div class="basket">
    <table class="item-list">
      <thead class="table-head">
        <tr>
          <th class="name-category">Product</th>
          <th class="name-category">Price</th>
          <th class="name-category">Quantity</th>
          <th class="name-category">Subtotal</th>
        </tr>
      </thead>
      <tbody class="table-body">
        @foreach (Cart::getContent() as $product)
          <tr class="board-line">
              <td scope="row" class="product-name-cart" id="row-product">
                <p class="name-cart-prod">{{ $product->model->name }}</p>
              </td>
              <td class="product-price-cart" id="row-product"><span>{{ $product->model->getEuropePrice() }}</span></td>

            <div class="change-quantity_basket">
           
              <td class="product-quantity-cart" id="row-product"><span>{{ $product->quantity }}</span></td>

            </div>
              <td class="product-subtotal-cart" id="row-product"><span>{{ $product->model->getEuropePrice() }}</span></td>
              <td>
                <form class="to-trash" method="POST" action="{{ route('cart.destroy', ['rowId'=>$product->id]) }}">
                  @csrf
                  @method('DELETE')
                  <button class="delete-product" id="delete" type="submit">&times;</button></td>
                </form>
          </tr>
        @endforeach
      </tbody>
    </table>
    <div class="empty-basket">
      <a class="empty-cart" id="buttonn" href="{{ route('empty.cart') }}">Empty cart</a>
    </div>

    <div class="coupon-and-instruction">   
      <div class="coupon-zone">
        <h3 class="coupon-code">Coupon code</h3>
        @if ($conditionCalculatedValue3 === 0)
          @if (Auth::check())
            <p class="text-cart">If you have a coupon code, please enter it in the box below. It could be interesting for you</p>
            {{-- Formulaire de validation du coupon --}}
            <form action="{{ route('store.coupon') }}" method="post" class="coupon-form">
                @csrf
              <input type="text" placeholder="Apply coupon" aria-describedby="button-addon3" class="input-coupon" name="code"> </br>
              <button class="apply-button" id="buton" type="submit">Apply coupon</button>
              </form>
          @else
            <p class="font-italic mb-4">If you want to have acces of coupon you'll need to create an account or login</p>
          @endif
        @else 
          <p class="font-italic mb-4">Coupon already applied</p>
        @endif
      </div>
        <div class="seller-information">
          <h3 class="seller-title">Instructions for seller</h3>
          <p class="text-cart">If you have some information for the seller you can leave them in the box below</p>
          <textarea name="" placeholder="Note the instructions" cols="30" rows="1" class="form-control"></textarea></br>
        </div>
    </div>
  </div>

  <aside class="item-summary">
      <h4>Cart total</h4>
      <div class="subtotal-and-total">
        <div class="subtotal-price"><strong class="text-muted">Order Subtotal</strong><span>{{ getPrice(Cart::getSubTotal())}}</span></div>
        <div class="subtotal-price">
          <strong class="text-muted">Coupon </strong>
        @if ($conditionCalculatedValue3 === 0)
            {{-- Show nothing --}}
        @else
          <form class="delete-form" action="{{ route('destroy.coupon') }}" method="post">
            @csrf
            @method('DELETE') 
            <button class="delete-coupon" id="delete" type="submit">&times;</button></td>
          </form>
        @endif
          <span> {{ getPrice($conditionCalculatedValue3)}}</span>
        </div>
        <div class="subtotal-price"><strong class="text-muted">Tax</strong><span>{{ getPrice($conditionCalculatedValue)}}</span></div>
        <div class="subtotal-price"><strong class="text-muted">Shipping</strong><span>{{  getPrice($conditionCalculatedValue2)}}</span></div>
        <div class="total-price">
          <strong class="text-muted">Total</strong>
          <span class="font-weight-bold">{{ getPrice(Cart::getTotal())}}</span>
        </div>
      </div>
      <a href="{{route ('checkout.index')}}" class="checkout-button" id="button">Procceed to checkout</a>
  </aside>
  
  @else
  <div id="empty-cart">
    <div class="empty_cart">
      <img src="{{asset('storage/images/panier_girl.svg') }}" alt="" class="shopping_girl"/>
      <h2 class="empty_basket">Your cart is empty</h2>
      <img src="{{asset('storage/images/basket_boy.svg') }}" alt="" class="shopping_girl"/>
    </div>
    <h3 class="next-step">Start your purchases to go to the next step</h3>
    <a class="start-purchase-btn" id="button" href="/category">See our products</a>
      <a class="start-purchase-btn" id="buton" href="/">Back to home</a>
  </div>
  @endif
</div>

   </main>
