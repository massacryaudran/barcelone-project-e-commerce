@extends('layouts.master')
<!DOCTYPE html>
<head>
    <html lang="en" >
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Consult the legal mention for any relative question">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}"> 
    <title>Bunty-G - Legal mention</title>
</head>


<main>
    <div class="legalMention-page">
        <h2 class="mention-title">Legal mention</h2>
        <div class="data-mention">
            <h3>Collection of personal data </h3>
            <p>
                The information collected on this form is stored in a computerized file by Bunty - G to allow the creation of an account and to be able to order on our website. The legal basis for processing is the execution of a contract and consent.
            </p>
            <p>
                The collected data will be communicated to the following recipients only: Bunty - G and delivery agents (only information relating to the smooth progress of the delivery).
            </p>
            <p>
                The data are kept for 3 years in accordance with the right to be forgotten <a href="https://gdpr.eu/right-to-be-forgotten/#:~:text=In%20Article%2017%2C%20the%20GDPR,originally%20collected%20or%20processed%20it." target="_blank" aria-label="GDPR website redirection"> <span class="info-mention" id="info-legal" >?</span></a>. with the possibility of deleting the account before the date. All delivery data will only be kept during the active period of the contract with mandatory deletion in the following month (The same contractual conditions are attached to the support page).
            </p>
            <p>
                You can access data about yourself, correct it, request its erasure or exercise your right to restrict the processing of your data. (Depending on the legal basis of the processing, also mention: You may withdraw your consent to the processing of your data at any time; You may also object to the processing of your data; You can also exercise your right to data portability)
            </p>
            <p>
                Visit <a href="https://www.cnil.fr/en/home" target="_blank" aria-label="CNIL website redirection">cnil.fr</a> for more information on your rights.
            </p>
            <p>
                To exercise these rights or for any questions about the processing of your data in this device, you can contact (if applicable, our data protection officer or the service responsible for exercising these rights): <a href = "mailto: Hello@bunty-g.com" aria-label="send an email to">Hello@bunty-g.com</a>.
            </p>
            <p>
                If you feel, after contacting us, that your “Information Technology and Freedoms” rights are not being respected, you can make a complaint to the CNIL.
            </p>
        </div>
        <div class="cgv-mention">
            <h3>General sales conditions</h3>
            <h4>1 - Right of retention</h4>
            <p>
                All delivered products remain the property of Bunty-G until delivered to the carrier.
            </p>
            <h4>2 - Legal right of withdrawal</h4>
            <p>
                You may withdraw your order without giving a reason within 14 days from the date on which you, or a third party designated by you (other than the carrier), took physical possession of the goods purchased (or the last good, lot or part if the contract relates to the delivery of several goods or lots or parts delivered separately) or the date on which you entered into the contract for the provision of services. To meet the withdrawal deadline, simply send your withdrawal request before the 14-day deadline expires and return your product through our online return center. For any further information on the scope, content and instructions regarding the exercise of your right of withdrawal, please contact us by the <a href="{{ route('index.support') }}" aria-label="support page redirection">support</a>.
            </p>
            <h4>3 - Effect of withdrawal</h4>
            <p>
                We will refund you all payments we have received from you, including standard delivery charges (i.e. the cheapest delivery we offer) no later than 14 days from receipt of your withdrawal request. We will use the same payment method that you used when you originally ordered unless you expressly agree to a different method. In any case, this refund will not result in additional costs for you. We may defer the refund until we have received the product(s) or you have provided proof of shipment of the product(s), whichever is earlier. If the refund is made after the deadline mentioned above, the amount due to you will be automatically increased. Please note that you must return the product(s) no later than 14 days from the date you notified us of your withdrawal decision.
                You must bear the direct costs of returning the product(s). You will be responsible for the depreciation of the value of the product(s) resulting from manipulations (other than those necessary to establish the nature, characteristics and proper functioning of this product(s)).
            </p>
            <h4>4 - Customs</h4>
            <p>
                When you order products on our website for delivery outside the European Union, you may be subject to obligations and import taxes, which are collected when the package arrives at its destination. Any additional customs clearance costs will be at your expense; we have no control over these costs. Customs policies vary greatly from country to country, so you should contact the local customs service for more information. Also, please note that when you place an order, you are considered the importer of record and must comply with all the laws and regulations of the country in which you receive the products. Your privacy is important to us and we draw the attention of our international customers to the fact that cross-border deliveries are likely to be opened and inspected by customs authorities.
            </p>
            <h4>5 - Liability guarantees</h4>
            <p>
            You benefit from the legal guarantee of conformity under the conditions of articles L.217-4 <a href="https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000044142575" target="_blank" aria-label="articles L.217-4 redirection"> <span class="info-mention" id="info-legal" >?</span></a> and following codes of consumption and warranty of hidden defects under the conditions provided for in articles 1641 <a href="https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000006441924/" target="_blank" aria-label="articles 1641 redirection"> <span class="info-mention" id="info-legal" >?</span></a> and following civil code. The legal guarantee of conformity applies independently of any commercial guarantee granted.

            We are committed to providing all the care in use in the profession for the implementation of the service offered to the client. However, our liability cannot be withheld in the event of delay or breach of our contractual obligations if the delay or breach is due to a cause outside our control: unforeseen circumstances or cases of force majeure as defined by applicable law.
            The limitation of liability referred to above shall not apply in the event of fraud or gross negligence on our part, in the event of bodily injury or liability for defective products, in the event of eviction and in the event of non-compliance (including hidden defects).
            </p>
            <h4>6 - Modification of services or general conditions of sale</h4>
            <p>
                We reserve the right to make changes to our Website, our procedures, and our terms and conditions, including these Terms and Conditions at any time. You are subject to the terms and conditions, procedures and General Conditions of Sale in force at the time you order a product from us, unless a change to these terms and conditions, or these Terms and Conditions of Sale are required by an administrative or governmental authority (in this case, this change may apply to previous orders you have placed). If any provision of these Terms and Conditions of Sale is deemed invalid, void or unenforceable for any reason, this provision shall be deemed divisible and shall not affect the validity and effectiveness of the remaining provisions.
            </p>
            <h4>7 - Rule violation</h4>
            <p>
                If you violate these Terms and Conditions and we do not take any action, we would still be entitled to use our rights and remedies in all other situations where you violate these Terms and Conditions.
            </p>
        </div>
    </div>
</main>