@extends('layouts.master')
<!DOCTYPE html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Leran some informations about the brand and team">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}"> 
    <title>Bunty-G - About us</title>
</head>
<main>
<div class="about-page">
    <h2>About us</h2>
    <div class="brand-presentation">
        <h3>Who are we ?</h3>
        <p>During and after covid, everyone wanted to do something that they can get their hands dirty as people cannot go outside and be with nature.Aware of this desire that many of us may feel, we decided to create our own brand of gardening, Bunty-G, based on gardening but also on the environment. By creating sustainable products made of recycled materials, we hope to benefit as many customers as possible while keeping an ecological aspect to strengthen our position on the issue of global warming.</p>
        <div class="team-member">
            <div class="member">
                <h4>Audran MASSACRY</h4>
                <img class="member-img" src="{{asset("storage/images/Audran.png")}}" alt="image of Audran MASSACRY"/>
            </div>
            <div class="member">
                <h4>Mathis ALBAN</h4>
                <img class="member-img" src="{{asset("storage/images/Mathis.png")}}" alt="image of Mathis ALBAN"/>
            </div>
            <div class="member">
                <h4>Sean RIDULME</h4>
                <img class="member-img" src="{{asset("storage/images/sEAN.jpg")}}" alt="image of Sean RIDULME"/>
            </div>
        </div>
    </div>
    <div class="presentation-work">
        <div class="work-content">
            <h3 class="work">How do we work ?</h3>
            <p class="work-text">In order to offer the best for you without impacting the environment, we have created all our products on the theme of sustainability. These consist of 3 materials: steel, wood and PET plastic. All come from the recycling chain and have been transformed to suit our tools. Each of the tools has been designed for you at first. Indeed, they want to be pleasant to hold, lightweight while being robust and effective in their task. They are also informative and easy to use without exorbitant cost. Of course, they also want to be sustainable. We want each of our tools to have as little impact on the environment as possible: whether during their creation, use or recycling. </p>
        </div>
    </div>
    <div class="brand-position">
        <h3>Our goals</h3>
        <p>When we talk about the environment, a number of goals come to mind for companies and consumers. As a gardening brand, our goals are clear and understandable to everyone.</p>
        <div class="brand-goals">
            <div class="goal">
                <h4>Be accessible</h4>
                <p>We want everyone who wants it to have easy access to our products (whether in an apartment or a house).</p>
            </div>
            <div class="goal">
                <h4>Raise awareness</h4>
                <p>Customers need a new way of thinking to reduce their impact on the environment.</p>
            </div>
            <div class="goal">
                <h4>Educate</h4>
                <p>Be able to teach beginners how to garden and become autonomous with their own culture.</p>
            </div>
            <div class="goal">
                <h4>Be sustainable</h4>
                <p>be able to meet our own needs with environmentally friendly and non-polluting tools.</p>
            </div>
        </div>
    </div>
</div>
</main>

<style>
    .presentation-work {
        background-image: url({{asset('/storage/images/background_img.jpg')}});
    }
</style>