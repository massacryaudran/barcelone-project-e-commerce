@extends('layouts.master')
<!DOCTYPE html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Checkout page">
  <link rel="stylesheet" href="{{ asset('css/app.css') }}"> 
  <title>Bunty-G - Checkout</title>
</head>
<main>
  <h2 class="checkout-title">Checkout Page</h2>
  <div class="checkout-page">
    <input class="tab-checkout" id="tab1" type="radio" name="tabs" checked>
    <label class="checkout-step" for="tab1"><span class="numberCircle">1</span><span>Customer Information</span></label>

    <input class="tab-checkout" id="tab2" type="radio" name="tabs" >
    <label class="checkout-step" for="tab2"><span class="numberCircle">2</span><span>Shipping</span></label>

    <input class="tab-checkout" id="tab3" type="radio" name="tabs">
    <label class="checkout-step" for="tab3"><span class="numberCircle">3</span><span>Payment</span></label>

    <section id="content1" class="tab-content checkout-section">
      <div class="user-info-checkout">
        <div class="user-info-check">
          <label class="label-checkout" for="name">Name</label>
          <input type="text" name="name" class="input-payement" aria-label="user name"/>
        </div> 
        <div class="user-info-check">
          <label class="label-checkout" for="last-name">Last name</label>
          <input type="text" name="last-name" class="input-payement" aria-label="user lastname"/>
        </div>  
        <div class="user-info-check">
          <label class="label-checkout" for="email-adress">Email</label>
          <input type="text" name="email-adress" class="input-payement" aria-label="user email"/>
        </div>  
        <div class="user-info-check">
          <label class="label-checkout" for="number_phone">Phone number</label>
          <input type="text" name="number_phone" class="input-payement" aria-label="user phone"/>
        </div> 
        <h5 class="legal-mention-checkout">For all questions related to personal data, see our legal mentions <a class="numberCircle" id="info-mention" href="{{ route('index.mention') }}" target="_blank">?</a></h5>  
      </div>
      <div class="button-master-container">
        <a class="button-container" href="{{ route('cart.index') }}">Return to cart</a>
        <label class="button-container" for="tab2"><span>Go to shipping</span></label>
      </div>
      
    </section>

    <section id="content2" class="tab-content checkout-section">
      <div class="user-shipping">
        <div class="user-ship-adress">
          <label class="label-checkout" for="ship-adress">Shipping adress</label>
          <input type="text" name="ship-adress" class="input-payement" aria-label="user adsress"/>
        </div> 
        <div class="user-ship-adress">
          <label class="label-checkout" for="ship-adress-second">Additional adress</label>
          <input type="text" name="ship-adress-second" class="input-payement" aria-label="user second address"/>
        </div> 
        <div class="shipping-complement">
          <div class="user-ship-adress2">
            <label class="label-checkout" for="ship-city">City</label>
            <input type="text" name="ship-city" class="input-payement" aria-label="user city"/>
          </div> 
          <div class="user-ship-adress2">
            <label class="label-checkout" for="ship-region">Region</label>
            <input type="text" name="ship-region" class="input-payement" aria-label="user region"/>
          </div> 
          <div class="user-ship-adress2">
            <label class="label-checkout" for="ship-postal">Postal code</label>
            <input type="text" name="ship-postal" class="input-payement" aria-label="user postal"/>
          </div> 
          <div class="user-ship-adress2">
            <label class="label-checkout" for="ship-country">Country</label>
            <input type="text" name="ship-country" class="input-payement" aria-label="user country"/>
          </div> 
        </div>
        <h5 class="legal-mention-checkout">For all questions related to personal data, see our legal mentions <a class="numberCircle" id="info-mention" href="{{ route('index.mention') }}" target="_blank">?</a></h5>
      </div>
      <div class="button-master-container">
        <label class="button-container" for="tab1"><span>Return to customer information</span></label>
        <label class="button-container" for="tab3"><span>Go to payment</span></label>
      </div>
    </section>

    <section id="content3" class="tab-content checkout-section">
    <div class="card-authorized">  
      <div class="card-content">
        <h4 class="card-title">Credit Card</h4>
        <p class="card-info">Safe money transfer using your bank account. Safe payment online. Credit card needed. Visa, Maestro, Discover, American Express</p>
      </div>
      <div class="card-logo">
        <div class="select-logo-sub logo-spacer">
          <img src="https://www.dropbox.com/s/by52qpmkmcro92l/logo-visa.png?raw=1" alt="Visa"/>
        </div>
        <div class="select-logo-sub">
          <img src="https://www.dropbox.com/s/6f5dorw54xomw7p/logo-mastercard.png?raw=1" alt="MasterCard"/>
        </div>
      </div>
    </div>
    <div class="payment-form">
      <div class="card-name">
        <label class="label-checkout" for="payement-name">Name on Card</label>
        <input type="text" name="payement-name" class="input-payement" aria-label="user name card"/>
      </div>  
      <div class="card-number">
        <label class="label-checkout" for="payement-number">Credit Card Number</label>
        <input type="text" name="payement-number" class="input-payement" maxlength="16" placeholder="XXXX XXXX XXXX XXXX" aria-label="user number card"/>
      </div>
      <div class="card-other">
        <div class="card-expiration">
          <label class="label-checkout" for="payement-date">Expiration Date</label>
          <select name="payement-date" class="choice-expiration" aria-label="user month expiration card">
            <option selected>MM</option>
            <option>01</option>
            <option>02</option>
            <option>03</option>
            <option>04</option>
            <option>05</option>
            <option>06</option>
            <option>07</option>
            <option>08</option>
            <option>09</option>
            <option>10</option>
            <option>11</option>
            <option>12</option>          
          </select>
          <select name="payement-date" class="choice-expiration" aria-label="user year expiration card">
            <option selected>YY</option>
            <option>22</option>
            <option>23</option>
            <option>24</option>
            <option>25</option>
            <option>26</option>           
          </select>
        </div>
        <div class="card-cvv">
          <label class="label-checkout" for="payement-cvv">CVV Code<span class="numberCircle" id="info-cvv" data-title="this is the 3 digits on the back of your card ">?</span></label>
          <input type="text" name="payement-cvv" class="input-payement" maxlength="3" placeholder="XXX" aria-label="user cvv card"/>
        </div>
      </div>
      <h5 class="legal-mention-checkout">For all questions related to personal data, see our legal mentions <a class="numberCircle" id="info-mention" href="{{ route('index.mention') }}" target="_blank">?</a></h5>  
    </div>
    <div class="button-master-container">
      <label class="button-container" for="tab2"><span>Return to shipping</span></label>
      <div class="button-container button-finish"><a href="#">Finish Order</a></div>
    </div>
  </section>
</div>
<script src="https://js.stripe.com/v3/"></script>
<script src="https://js.stripe.com/v2/"></script>

<script src="{{ asset('js/stripe.js') }}"></script>


</main>
