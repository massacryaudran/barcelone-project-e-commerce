<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="register to our website">
  <title>Bunty-G - Register</title>
  <link rel="stylesheet" href="{{ asset('css/login.css') }}"> 
</head>
<body>
<div class="container-login">
  <div class="img_decoration">
      <img src="{{ asset('/storage/images/flower_login.svg')}}" class="img_login" alt="flower logo">
    </div>
    <div class="img_decoration">
      <img src="{{ asset('/storage/images/flower_login.svg')}}" class="img_login2" alt="flower logo">
    </div>
 <form method="POST" action="{{ route('register') }}">
                        @csrf
   <a class="home" id="button" href="/">Back to home</a>
  <h1 class="a11y-hidden">Login Form</h1>

  <div>
    <label class="label-name">
      {{-- <input type="email" class="text" name="email" placeholder="Email" tabindex="1" required /> --}}
      
        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
      @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
      <span class="required">Name</span>
    </label>
  </div>
  <div>
     <label class="label-name">


<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

@error('email')
<span class="invalid-feedback" role="alert">
    <strong>{{ $message }}</strong>
</span>
@enderror
 <span class="required">{{ __('Email Address') }}</span>
</label>
  </div>
  <input type="checkbox" onclick="showPassword();" name="show-password" class="show-password a11y-hidden" id="show-password" tabindex="0" />
  <label class="label-show-password" for="show-password">
    <span>Show Password</span>
    
  </label>
  
  <div>
    <label class="label-password">
      {{-- <input type="text" class="text" name="password" placeholder="Password" tabindex="2" required /> --}}
        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

    @error('password')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
      <span class="required">Password</span>
    </label>
  </div>
<div>
     <label class="label-password">
      
    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
     <span class="required">{{ __('Confirm Password') }}</span>
    </label>

</div>


   <div class="form-check">
    <input class="form-check_check" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

    <label class="remeber_label" for="remember">
      <span>{{ __('Remember Me') }}</span>
    </label>
</div>

   <input type="submit" value=" {{ __('Register') }}">
   
  <div class="email">
    @if (Route::has('password.request'))
    <a class="btn btn-link" href="{{ route('password.request') }}">
        {{ __('Forgot Your Password?') }}
    </a>
    @endif
    
  </div>
  
  <figure aria-hidden="true">
    <div class="person-body"></div>
    <div class="neck skin"></div>
    <div class="head skin">
      <div class="eyes"></div>
      <div class="mouth"></div>
    </div>
    <div class="hair"></div>
    <div class="ears"></div>
    <div class="shirt-1"></div>
    <div class="shirt-2"></div>
  </figure>
</form>
</div>

</body>
</html>
<script>
  //Function to show password
function showPassword() {
  var x = document.getElementById("password");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
   var z = document.getElementById("password-confirm");
  if (z.type === "password") {
    z.type = "text";
  } else {
    z.type = "password";
  }

} 
  </script>