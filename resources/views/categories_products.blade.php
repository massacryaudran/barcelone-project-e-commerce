@extends('layouts.master')
<!DOCTYPE html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Discover our different categories of sustainable products">
  <script src="{{ mix('js/app.js') }}" defer></script>
  <link rel="stylesheet" href="{{ asset('css/app.css') }}"> 
  <link rel="stylesheet" href="{{ asset('css/categorypannel.css') }}">
  <title>Bunty-G - Category</title>  
</head>

<noscript>
You need to enable JavaScript to run this app.
</noscript>

<main>
	 <form method="post" action="{{ route('category.pannel',['slugurl'=>'gardening_tools']) }}">
      @csrf
        <input type="hidden" name="category_id" value="1">
          <i><button style="display:none;" type="submit" id="gardening_tools"><b>Views our Gardening tools</b></button></i>
    </form>
    <form method="post" action="{{ route('category.pannel',['slugurl'=>'flowers_pots']) }}">
      @csrf
        <input type="hidden" name="category_id" value="2">
          <i><button style="display:none;" id="flowers_pots" type="submit"><b>Views our Flowers pots</b></button></i>
    </form>
    <form method="post" action="{{ route('category.pannel',['slugurl'=>'seasonnal_books']) }}">
      @csrf
        <input type="hidden" name="category_id" value="3">
          <i><button style="display:none;" id="seasonnal_books" type="submit"><b>Views our seasonnal books</b></button></i>
    </form>
    <form method="post" action="{{ route('category.pannel',['slugurl'=>'fertilizers']) }}">
      @csrf
        <input type="hidden" name="category_id" value="4">
          <i><button style="display:none;" id="fertilizers" type="submit"><b>Views our fertilizers</b></button></i>
    </form>
    <form method="post" action="{{ route('category.pannel',['slugurl'=>'seasonnal_seed']) }}">
      @csrf
        <input type="hidden" name="category_id" value="5">
          <i><button style="display:none;" id="seasonnal_seed" type="submit"><b>Views our seasonnal seeds</b></button></i>
    </form>
    <form method="post" action="{{ route('category.pannel',['slugurl'=>'seasonnas_boxs']) }}">
      @csrf
        <input type="hidden" name="category_id" value="6">
          <i><button style="display:none;" id="seasonnal_boxs" type="submit"><b>Views our seasonnal boxs</b></button></i>
    </form>
					               
  	<div id="category"></div>
</main>
