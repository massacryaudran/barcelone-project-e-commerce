@extends('layouts.master')
<!DOCTYPE html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="View your account informations">
  <script src="{{ asset('js/account.js') }}" defer type="module"></script> 
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">   
  <title>Bunty-G - Account</title>
</head>
  
<main>
  <div class="account-page">
    <div class="logout-btn">
      <a class="logout_text" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
        {{ __('Logout') }}
      </a>
    </div>

    <div class="logout-btn">
    <a class="logout_text" href="/account/del/{{ $users->id }}">Remove your account</a>
    </div>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
      @csrf
    </form>
    <h2 class="title_info">Account Details</h2>
    <div class="container_account">
      <form action="{{ route ('account.modify')}}" method="POST" class="account">
      @csrf
      <div class="user_info">  
          <div class="container_info">
              <label for="name" class="label_info">First name: </label>
              <div class="container_input">
                <input type="text" name="name" id="name" value="{{ $users->name }}" class="input_info" readonly="readonly">
                  <img src="{{ asset('/storage/images/crayon.svg') }}" class="modify_tool" alt="Image" id="image">
              </div>
          </div>
          <div class="container_info">
              <label for="lastname" class="label_info">Last name: </label>
              <div class="container_input">
                <input type="text" name="lastname" id="lastname" value="{{ $users->lastname }}" class="input_info" readonly="readonly">
                  <img src="{{ asset('/storage/images/crayon.svg') }}" class="modify_tool" alt="Image" id="image2">
              </div>
          </div>
          <div class="container_info">
              <label for="email" class="label_info">Email: </label>
              <div class="container_input">
                <input type="text" name="email" id="email" value="{{ $users->email }}" class="input_info" readonly="readonly">
                  <img src="{{ asset('/storage/images/crayon.svg') }}" class="modify_tool" alt="Image" id="image3">
              </div>
          </div>
          <div class="container_info">
              <label for="phone_number" class="label_info">Phone number: </label>
              <div class="container_input">
                <input type="text" name="phone_number" id="phone_number" value="{{ $users->phone_number }}" class="input_info" id="intTextBox" readonly="readonly">
                  <img src="{{ asset('/storage/images/crayon.svg') }}" class="modify_tool" alt="Image" id="image4">
              </div>
          </div>
          <button class="submit_info" id="button" type="submit">Save</button>
          <input type="hidden" name="id" value="{{ $users->id }}">
      </div>
    </form>
    </div>
  </div>
</main>

