@extends('layouts.master') 
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="description" content="Interested in gardening? Shop with us for sustainable and eco-friendly tools! ">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">    
    <script src="{{ asset('js/tutoslide.js') }}" defer></script> 
    <title>Bunty-G - How to start</title>
</head>

<main class="tuto">
<div class="slider">
    <div id="previous-move" class="move" onclick="plusSlides(-1)">&#10094;</div>
    <div id="slider">
        <div class="slide slide0">
            <div class="slide-content">
                <h2>Equipment required</h2>
                <p>
                    To be able to follow the tutorial from home, it is essential to have the following tools: a transplanter, a watering can, a pot, seeds (or plants) and potting soil.
                </p>
            </div>
        </div>
        <div class="slide slide1">
            <div class="slide-content">
                <h2>Step 1 - Find the perfect spot</h2>
                <p>
                    Like any living being, plants have their own need to flourish. Nevertheless, it is important to note that each plant has its own needs! Even though they all need light and water, their quantity in terms of need varies as well as the environment they need. 
                    Some plants will need a sluggish soil while others will need a harder soil. The same applies to their other needs: a bright or shady place, a considerable need for water or once every two weeks...
                    Know that if one of the needs of the plant is not respected, it can come by getting sick or even dying. To know the needs of your future plant, you can refer to our books. 
                </p>
                <div>
                    <form method="post" action="{{ route('category.pannel',['slugurl'=>'seasonnal_books']) }}">
                    @csrf
                    <input type="hidden" name="category_id" value="3">
                    <i><button class="view-category" type="submit"><b>Views our books</b></button></i>
                    </form>
                </div>
            </div>
        </div>
        <div class="slide slide2">
            <div class="slide-content">
                <h2>Step 2 - Position the pot and place the soil</h2>
                <p>
                    Once the location is found, make sure to position your pot on the desired location. Here, the size of the pot is not important: it depends on what you want to plant. Once this is done, prepare your potting soil. 
                    If it's a seed, fill your pot at 3/4 (if it's a gardener containing several seeds, you can arrange them every 7 to 8 cm).
                    If it's a plant, fill your pot one-quarter or one-half depending on its size (if it's a gardener, you can space the plants 9-10 cm apart). 
                </p>
                <div>
                    <form method="post" action="{{ route('category.pannel',['slugurl'=>'flowers_pots']) }}">
                    @csrf
                    <input type="hidden" name="category_id" value="2">
                    <i><button class="view-category" type="submit"><b>Views our pots</b></button></i>
                    </form>
                </div>
            </div>
        </div>
        <div class="slide slide3">
            <div class="slide-content">
                <h2>Step 3 - Prepare the seed / plant</h2>
                <p>
                    In order to plant for optimal growth, it is necessary to follow the needs of the plant. 
                    For the seed, you just have to dig a small groove in the soil already laid. You will come, then put the seed there. 
                    For the plant, the hole must be 1.5 to 2 times larger than the diameter of the plant in order for it to develop properly. If you put too much soil in the previous step, you will have to remove it (the foot of the plant should be about 3/4 of the pot). Once this step is done, you will have to squeeze the stray pot (you can help yourself from the transplater) which contains the plant in order to detach the roots stuck to the pot as well as to split the earth to let the plant breathe. Place there then in the final pot.
                </p>
                <div>
                    <form method="post" action="{{ route('category.pannel',['slugurl'=>'seasonnal_seeds']) }}">
                    @csrf
                    <input type="hidden" name="category_id" value="5">
                    <i><button class="view-category" type="submit"><b>Views our seeds</b></button></i>
                    </form>
                </div>
            </div>
        </div>
        <div class="slide slide4">
            <div class="slide-content">
                <h2>Step 4 - Cover with potting soil</h2>
                <p>
                    Now you have to cover it with soil. For the seed, make sure to put a layer about 2 to 3 times the size of the soil. For the plant, cover all the holes until you reach the 3/4 of the pot. Tap with the transplanter to minimize the soil.  
                </p>
                <div>
                    <form method="post" action="{{ route('category.pannel',['slugurl'=>'fertilizers']) }}">
                    @csrf
                    <input type="hidden" name="category_id" value="4">
                    <i><button class="view-category" type="submit"><b>Views our fertilizers</b></button></i>
                    </form>
                </div>
            </div>
        </div>
        <div class="slide slide5">
            <div class="slide-content">
                <h2>Step 5 - Sprinkle</h2>
                <p>
                    Once the steps have been completed, you only have to water according to the needs of the plant. You can see your first results from a few days to a few weeks depending on the seed. If you wish, you can add a thin layer of fertilizer for a faster result. 
                    All you have to do is observe your interior or exterior bloom ! 
                </p>
                <div>
                    <form method="post" action="{{ route('category.pannel',['slugurl'=>'gardening_tools']) }}">
                    @csrf
                    <input type="hidden" name="category_id" value="1">
                    <i><button class="view-category" type="submit"><b>Views our gardening tools</b></button></i>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="next-move" class="move" onclick="plusSlides(1)">&#10095;</div>
</div>
</main>
<style>

  
    .slide0 {
        background-image: url({{asset('/storage/images/work.jpg')}});
    }
    .slide1 {
        background-image: url({{asset('/storage/images/tutorial_step1.jpg')}});
    }
    .slide2 {
        background-image: url({{asset('/storage/images/tutorial_step2.jpg')}});
    }
    .slide3 {
        background-image: url({{asset('/storage/images/tutorial_step3.jpg')}});
    }
    .slide4 {
        background-image: url({{asset('/storage/images/tutorial_step4.jpg')}});
    }
    .slide5 {
        background-image: url({{asset('/storage/images/tutorial_step5.jpg')}});
    }
</style>

