<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Audran and Mathis">
	<meta http-equiv="Cache-control" content="no-cache">
	<meta http-equiv="Expires" content="-1">
    <script src="https://code.jquery.com/jquery-3.6.1.min.js"></script>
    <script src="{{ asset('js/menuslide.js') }}" defer type="module"></script> 
	<script src="{{ asset('js/closeAlert.js') }}" type="module"></script>
	<script src="{{ asset('js/loader.js') }}" type="module"></script>
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">   
	<script src="{{ mix('js/app.js') }}" defer></script>
  </head>

<body>
	<div id="loader">
		<div class="dots">
			<div style="--d:0"></div>
			<div style="--d:1"></div>
			<div style="--d:2"></div>
			<div style="--d:3"></div>
			<div style="--d:4"></div>
		</div>
	</div>

	{{-- Notif --}}
	@if (session('success'))
  		<div class="alert alert-succes">
    		<button class="close">&times;</button>
    			{{session('success')}} 
  		</div>
    @endif
    @if (session('error'))
  		<div class="alert alert-error">
    		<button class="close">&times;</button>
    			{{session('error')}} 
  		</div>
    @endif
{{-- Menu content --}}
	<a href="#cd-nav" class="cd-nav-trigger" aria-label="display navbar buton">
		<span class="cd-nav-icon"></span>
		<svg x="0px" y="0px" width="54px" height="54px" viewBox="0 0 54 54">
			<circle fill="transparent" stroke="#656e79" stroke-width="1" cx="27" cy="27" r="25" stroke-dasharray="157 157" stroke-dashoffset="157"></circle>
		</svg>
	</a>

	<div id="cd-nav" class="cd-nav">
   		<div class="cd-navigation-wrapper">
			  <div class="search-bar">
        @include('partials.search_products') 
      </div>
   			<div class="logo_container">
        		<a href={{ route ("product.pannel")}}><img src="{{ asset('storage/images/logo.svg') }}" width="200px" height="200px" id="logo_btn"></a><!-- .cd-half-block -->
			</div>
			<div class="cd-half-block">
   				<h2>Navigation</h2>
            <nav>
				<ul class="cd-primary-nav">
					<li>
						<a href="{{ route('category_index') }}"><button id="menu_btn" type="submit"><b>Products</b></button></a>
					</li>
					<li>
						<a href="{{ route('index.tutorial') }}"><button id="menu_btn" type="submit"><b>How to start</b></button></a>
					</li>
					<li>
						<a href="{{ route('make.my.box') }}"><button id="menu_btn" type="submit"><b>Make my Box</b></button></a>
					</li>
					<li class="responsive-laptop">
						<a href="{{ route('cart.index') }}"><button id="menu_btn" type="submit"><b>Basket</b></button></a>
					</li>
					<li>
						<a href="{{ route('index.about') }}"><button id="menu_btn" type="submit"><b>About Us</b></button></a>
					</li>
					<li>
						<a href="{{ route('index.support') }}"><button id="menu_btn" type="submit"><b>Support</b></button></a>
					</li>
					@if (Auth::check())   
					<li class="responsive-laptop" id="item1">
						<a href="{{ route('account.index') }}"><button id="menu_btn" type="submit"><b>Account</b></button></a>
					</li>  
					@else
					<li class="responsive-laptop" id="item1">
						<a href="/login"><button id="menu_btn" type="submit"><b>Login</b></button></a>
					</li>
					@endif  
					
					
					
				</ul>
			</nav>
    
			</div>
     
			<div class="cd-half-block">
				<address>
					<h2>Contact</h2>
					<ul class="cd-contact-info">
						<li><a href="mailto:info@myemail.co">Hello@bunty-g.com</a></li>
						<li>Grow at home !</li>
						<li>
							<span>Bogoss street</span>
							<span>W1234X</span>
							<span>Perrex, FR</span>
						</li>
						<li><a href="{{ route('index.mention') }}">Legal Mention</a></li>
					</ul>
				</address>
				
				
			</div> <!-- .cd-half-block -->
			
		</div> <!-- .cd-navigation-wrapper -->

		<div class="container_account_basket" id="nav-responsive">    
      @if (Auth::check())     
        <a class="link" href="{{ route('account.index') }}"><img class="img_link" src="{{ asset('/storage/images/girl.svg') }}" alt="account page" class="img-basket4"></a><span class="login-home">Account</span>
        @if ($cartCollection->count() > 0)
          <a class="cart" href="/cart">
            <img class="img_link" src="{{ asset('/storage/images/basket2.svg') }}" alt="basket page">    
          </a>
        <span class="count-basket">{{ $cartCollection->count() }}</span>
          @else
            <a class="cart" href="/cart">
              <img class="img_link" src="/storage/images/empty_basket.svg" alt="basket page">    
            </a>
          <span class="count-basket">{{ $cartCollection->count() }}</span>
          @endif
        @else
          <a class="link" href="/login" class="link" aria-label="Login">
            <img class="img_link" src="/storage/images/girl_normal.svg" alt="login page">      
          </a>
            <span class="login-home">Login</span>
              @if ($cartCollection->count() > 0)
              <a class="cart" href="/cart">
                <img class="img_link" src="{{ asset('/storage/images/basket2.svg') }}" alt="basket page">    
              </a>
                <span class="count-basket">{{ $cartCollection->count() }}</span>
              @else
                <a class="cart" href="/cart">
                  <img class="img_link" src="/storage/images/empty_basket.svg" alt="basket page">    
                </a>
                 <span class="count-basket">{{ $cartCollection->count() }}</span>
              @endif
      @endif   
    </div>

		<footer class="website-footer">
		<div class="first-line">
			<div class="brand-logo">
				<img class="footer-logo" src="{{ asset('/storage/images/logo.svg')}}">
				<p>Bunty - G</p>
			</div>
			<div class="all-pages">
				<a href="https://www.linkedin.com/in/audran-massacry-cybersecurite/" target="_blank">Audran MASSACRY</a>
				<a href="https://www.linkedin.com/in/mathis-alban-informatic-student/" target="_blank">Mathis ALBAN</a>
				<a href="https://www.linkedin.com/in/sean-ridulme/" target="_blank">Sean RIDULME</a>
			</div>
			<div class="something">
				<a href="{{ route('index.mention') }}">Legal mention</a>
			</div>
		</div>
		<div class="second-line">
			<p class="copyright">Copyright & copy | 2022 All Rights Reserved by Bunty-G</p>
		</div>
		</footer>
	</div> <!-- .cd-nav -->
</body>
</html>


 