@extends('layouts.master')
<head>
  <meta name="description" content="make your own box">
  <title>Bunty-G - Box maker</title>
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">  
  <script src="{{ mix('js/app.js') }}" defer></script>
</head>
<main>
  <div class="container_makerbox_end">
    <div class="container_box_info_end">
      <h4>Box :</h4>
      <span>Name :{{$boxmaker->boxname}}</span>
      <span>From :{{$boxmaker->creator}}</span>
      <span>Size :{{$boxmaker->size}}</span>
      <span>Option 1 :{{$boxmaker->seeds}}</span>
      <span>Option 2 :{{$boxmaker->books}}</span>
    </div>
  </div>
</main>