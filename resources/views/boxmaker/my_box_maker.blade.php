@extends('layouts.master')
<!DOCTYPE html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="make your own box">
  <title>Bunty-G - Box maker</title>
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">  
  <script src="{{ mix('js/app.js') }}" defer></script>
</head>

  <main>
    @if (Auth::check()) 
      <div class="boxContainer-page">
        <form class="box-stepOne" method="post" action="{{ route('step2') }}">
          @csrf
            <h2 class="h2_box">Box maker</h2>
            <h4 class="h3_box">First step :</h4>
            <div class="name-your-box">
              <input id="boxname" name="boxname" type="text" required>
              <label class="label-name" for="boxname">Name your box</label>
            </div>
            <div class="name-the-person">
              <input id="creator" name="creator" type="text" required>
              <label class="label-creator" for="creator">This box is for who ?</label>
            </div>
            <div class="custom-select"> 
              <select class="size-choice" name="size" id="size">
                <option value="0" selected>Choose the size of your box</option>
                <option value="Small">Small</option>
                <option value="Medium">Medium</option>
                <option value="Large">Large</option>
              </select>
            </div>
            <div class="div_button">
            <a href="{{ route('step2')}}"><button class="submit_box">Next Step</button></a>
            </div>
        </form>
        <b>Features still in development</b>
      </div>
    @else
      <div class="logalert">
        <h2>You need to be logged to create your boxs</h2>
        <h3>Log in or create an account to continue on the page</h3>
        <span class="button-box">
          <a class="boxMaker-connect" id="button" href="/login" aria-label="login page redirection">Log in</a>
          <a class="boxMaker-connect" id="buton" href="/register" aria-label="register page redirection" >Register</a>
        </span>
      </div>
    @endif
  </main>