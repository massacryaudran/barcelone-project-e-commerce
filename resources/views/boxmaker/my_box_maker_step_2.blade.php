@extends('layouts.master')
<head>
  <meta name="description" content="make your own box">
  <title>Bunty-G - Box maker</title>
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">  
  <script src="{{ mix('js/app.js') }}" defer></script>
</head>

<main class="boxmaker-main">
  <div class="container_box_info">
    <h4>Box :</h4>
    <div class="box_span">
    <span>Name :{{$boxmaker->boxname}}</span>
    <span>From :{{$boxmaker->creator}}</span>
    <span>Size :{{$boxmaker->size}}</span>
    </div>
  </div>
<form class="" method="post" action="{{route('step3')}}">
  @csrf
    <div class="container_box">
      <div id="boxmakerstep" class="boxmakerstep"></div>
        </div>
      <div class="container_box_select">
        <div class="custom-select_box"> 
          <select class="size-choice" name="seeds" id="seeds">
            <option value="0" selected>Choose wich seeds do you want</option>
            <option value="Summer Seeds">Summer Seeds</option>
            <option value="Winter Seeds">Winter Seeds</option>
            <option value="Spring Seeds">Spring Seeds</option>
          </select>
        </div>
          <div class="custom-select_box"> 
          <select class="size-choice" name="books" id="books">
            <option value="0" selected>Choose wich books do you want</option>
            <option value="Summer Books">Summer Books</option>
            <option value="Winter Books">Winter Books</option>
            <option value="Spring Books">Spring Books</option>
          </select>
        </div>
        <input type="hidden" name="id" value="{{ $userId }}" />
        <button type="submit" class="btn_boxmaker">Next Step</button>
     </div>
</form>
</main>
