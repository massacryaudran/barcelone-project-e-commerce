@extends('layouts.master')
<!DOCTYPE html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="For a great garden, discover our {{$product->name}}">
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">   
  <title>Bunty-G - {{$product->name}}</title>

  <script src="{{ mix('js/app.js') }}" defer></script>
  <script src="{{ asset('js/product-quant-gestion.js') }}"></script>
</head>
 
<style>
      canvas {
        position: relative;
      } 
      #canvas-container {
         position: relative;   
      }
      .scene {
        position: relative;
         border: 1px solid rgba(10, 207, 79, 0.414);
        border-radius: 20px;
        box-shadow: 0 0 10px rgba(0, 55, 16, 0.547)  
      }
</style> 



<main>
<div class=product-page>
  <div class="product-model-3D">
    <h2 class="product-name-p">{{$product->name}}</h2>
    <p class="date-creation-p">{{$product->created_at->format('d/m/Y')}}</p>
    <div class="3D-prod">
      <div class="prod-img-3D" id="{{$product->slug}}"> </div>
      <div class="ribbon"><p>{{$product->getEuropePrice()}}</p></div>
    </div>
    
  </div>
  <div class="product-content">
    <h2 class="product-name-p">{{$product->name}}</h2>
    <p class="date-creation-p">{{$product->created_at->format('d/m/Y')}}</p>
    <div class="description-page">
      <h3>Description</h3>
      <p class="product-description-page">{{$product->description}}</p>
    </div>
    <div class="characteristic-page">
      <h3>Characteristic</h3>
      <div class="display-characteristic">
        <div class="characteristic characteristic1">
          
          <img src="{{ asset('storage/images/color1.png')}}" class="characteristic-img" alt="product color
           logo">
          
          <p class=>{{$product->characteristic_color}}</p>
          
        </div>
        <div class="characteristic characteristic2">
          <img src="{{ asset('storage/images/material.png')}}" class="characteristic-img" alt="product materials logo">
          <p class=>{{$product->characteristic_materials}}</p>
        </div>
        <div class="characteristic characteristic3">
          <img src="{{ asset('storage/images/dimension.png')}}" class="characteristic-img" alt="product dimension logo">
          <p class=>{{$product->characteristic_dimension}}</p>
        </div>
        <div class="characteristic characteristic4">
          <img src="{{ asset('storage/images/weight1.png')}}" class="characteristic-img" alt="product weight logo">
          <p class=>{{$product->characteristic_weight}}</p>
        </div>
      </div>
      <form method="POST" action="{{ route('cart.store') }}" class="choose-number">
        @csrf
        <div class="change-quantity">
          <div class="value-button" id="decrease" onclick="decreaseValue()" value="Decrease Value"><</div>
          <input class="product-quantity" id="number" type="number" name="quantity" inputmode="numeric" min="1" max="10" value="1" aria-label="product quantity gestion">
          <div class="value-button" id="increase" onclick="increaseValue()" value="Increase Value">></div>
        </div>
        <input type="hidden" name="product_id" value="{{ $product->id }}">
        <input type="hidden" name="product_id" value="{{ $product->id }}">
          {{-- <input type="hidden" name="name" value="{{ $product->name }}">
          <input type="hidden" name="price" value="{{ $product->price }}"> --}}
        <button class="button-p" id="button" type="submit">Add to cart</button>
      </form>
    </div>
  </div>
</div>
</main>
<style>
  @media (max-width: 600px) {
  .scene{
    width: 100% !important;  
  }
}
  </style>