<?php

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CartController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SupportController;
use App\Http\Controllers\BoxMakerController;
use App\Http\Controllers\CheckoutController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// |--------------------------------------------------------------------------
// Admin pannel routes
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});


// |--------------------------------------------------------------------------
// Product routes

// Show all the product in random order
Route::get('/', [ProductController::class, 'index'])->name('product.pannel');
// Search products
Route::get('/product/search', [ProductController::class, 'search'])->name('products.search');
// Show all the tools products
Route::post('/product/{slugurl}', [ProductController::class, 'category_show'])->name('category.pannel');
// Show a product
Route::get('/product/{slug}', [ProductController::class, 'show'])->name('product.show');

Route::get('/category', [ProductController::class, 'category'])->name('category_index');

// |--------------------------------------------------------------------------
// Page routes

//Redirect to make my box page
Route::get('/make_my_box', [BoxMakerController::class, 'make_my_box'])->name('make.my.box');
//Redirect to about us page
Route::get('/about_us', [Controller::class, 'about_us'])->name('index.about');
//Redirect to legal mention page
Route::get('/legal_mention', [Controller::class, 'legal_mention'])->name('index.mention');
//Redirect to tutorial page
Route::get('/tutorial', [Controller::class, 'how_to_start'])->name('index.tutorial');
//Redirect to support page
Route::get('/support', [SupportController::class, 'support'])->name('index.support');
// Send mail from the support pages
Route::post('/support/sendmail', [SupportController::class, 'sendMail'])->name('send-mail');


// |--------------------------------------------------------------------------
// Cart routes

// Add product
Route::post('/cart/add', [CartController::class, 'store'])->name('cart.store');
// Clear the cart
Route::get('/cart/empty', [CartController::class, 'emptycart'])->name('empty.cart');
// Show the cart
Route::get('/cart', [CartController::class, 'show'])->name('cart.index');
// Add a coupon 
Route::post('/store_coupon', [CartController::class, 'storeCoupon'])->name('store.coupon');
// Remove a coupon
Route::delete('/destroy_coupon', [CartController::class, 'destroyCoupon'])->name('destroy.coupon');
// Destroy one item from the cart
Route::delete('/cart/{rowId}', [CartController::class, 'destroy'])->name('cart.destroy');

Route::patch('/cart/update/{powId}', [CartController::class, 'update_quantity'])->name('cart.update');






// Checkout routes

Route::get('/checkout', [CheckoutController::class, 'index'])->name('checkout.index');

Route::post('/checkout_complete', [CheckoutController::class, 'complete'])->name('checkout.create');

// |--------------------------------------------------------------------------
// Account routes
// Show account info
Route::get('/account', [AccountController::class, 'index'])->name('account.index');
// Modify account info
Route::post('/account_modify', [AccountController::class, 'modify'])->name('account.modify');

Route::get('/account/del/{id}', [AccountController::class, 'del'])->name('deluser');
// |--------------------------------------------------------------------------
// Auth routes

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



// |--------------------------------------------------------------------------
// Box maker Routes
// Show account info
Route::post('/boxmaker', [BoxMakerController::class, 'step2'])->name('step2');

Route::post('/boxmaker_step3', [BoxMakerController::class, 'step3'])->name('step3');

