"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_categoryviewer_Category_js"],{

/***/ "./resources/js/categoryviewer/Category.js":
/*!*************************************************!*\
  !*** ./resources/js/categoryviewer/Category.js ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Model)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _react_three_drei__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @react-three/drei */ "./node_modules/@react-three/drei/core/useGLTF.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





function Model(props) {
  var _useGLTF = (0,_react_three_drei__WEBPACK_IMPORTED_MODULE_2__.useGLTF)("/storage/objects/category/category.gltf"),
      nodes = _useGLTF.nodes,
      materials = _useGLTF.materials;

  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", _objectSpread(_objectSpread({}, props), {}, {
    dispose: null,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("group", {
      position: [35, 0, -28],
      rotation: [Math.PI / 2, 0, 0],
      scale: 1,
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
        geometry: nodes.Mesh010.geometry,
        material: materials.leaves10
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
        geometry: nodes.Mesh010_1.geometry,
        material: materials.PLANT02
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
        geometry: nodes.Mesh010_2.geometry,
        material: materials.pot_m
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
        geometry: nodes.Mesh010_3.geometry,
        material: materials.plank_box
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
        geometry: nodes.Mesh010_4.geometry,
        material: materials.shovel_m
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
        geometry: nodes.Mesh010_5.geometry,
        material: materials["Black plastic old scratched"]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
        geometry: nodes.Mesh010_6.geometry,
        material: materials.PLANT01
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
        geometry: nodes.Mesh010_7.geometry,
        material: materials.dirt
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
        geometry: nodes.Mesh010_8.geometry,
        material: materials.pot
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
        geometry: nodes.Mesh010_9.geometry,
        material: materials["Material.001"]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
        geometry: nodes.Mesh010_10.geometry,
        material: materials["Scene_-_Root"]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
        geometry: nodes.Mesh010_11.geometry,
        material: materials.lambert1
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
        geometry: nodes.Mesh010_12.geometry,
        material: materials.leaves1
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
        geometry: nodes.Mesh010_13.geometry,
        material: materials.leaves2
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
        geometry: nodes.Mesh010_14.geometry,
        material: materials.leaves3
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
        geometry: nodes.Mesh010_15.geometry,
        material: materials.leaves5
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
        geometry: nodes.Mesh010_16.geometry,
        material: materials.leaves6
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
        geometry: nodes.Mesh010_17.geometry,
        material: materials.leaves9
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
        geometry: nodes.Mesh010_18.geometry,
        material: materials.Material
      })]
    })
  }));
}
_react_three_drei__WEBPACK_IMPORTED_MODULE_2__.useGLTF.preload("/storage/objects/category/category.gltf");

/***/ })

}]);