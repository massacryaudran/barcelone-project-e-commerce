//close alert by pressing the close button

$(".close").click(function () {
    $(this).parent(".alert").fadeOut();
});

//close alert after a 6s time out 

setTimeout(function () {
    $(".close").parent(".alert").fadeOut();
}, 6000);
