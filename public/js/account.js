// Filter input

function setInputFilter(textbox, inputFilter, errMsg) {
    [
        "input",
        "keydown",
        "keyup",
        "mousedown",
        "mouseup",
        "select",
        "contextmenu",
        "drop",
        "focusout",
    ].forEach(function (event) {
        textbox.addEventListener(event, function (e) {
            if (inputFilter(this.value)) {
                // Accepted value
                if (["keydown", "mousedown", "focusout"].indexOf(e.type) >= 0) {
                    this.classList.remove("input-error");
                    this.setCustomValidity("");
                }
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                // Rejected value - restore the previous one
                this.classList.add("input-error");
                this.setCustomValidity(errMsg);
                this.reportValidity();
                this.value = this.oldValue;
                this.setSelectionRange(
                    this.oldSelectionStart,
                    this.oldSelectionEnd
                );
            } else {
                // Rejected value - nothing to restore
                this.value = "";
            }
        });
    });
}

window.addEventListener("load", function (e) {
    setInputFilter(
        document.getElementById("phone_number"),
        function (value) {
            return /^-?\d*$/.test(value);
        },
        "Must be an integer"
    );
});

// Input block
function inputBlock() {
    document.getElementById("image").addEventListener("click", function (e) {
        var input = document.getElementById("name");
        input.readOnly = false;
        addEventListener("focusout", (event) => {
            input.readOnly = true;
        });
    });
}
inputBlock();

function inputBlock1() {
    document.getElementById("image2").addEventListener("click", function (e) {
        var input = document.getElementById("lastname");
        input.readOnly = false;
        addEventListener("focusout", (event) => {
            input.readOnly = true;
        });
    });
}
inputBlock1();

function inputBlock2() {
    document.getElementById("image3").addEventListener("click", function (e) {
        var input = document.getElementById("email");
        input.readOnly = false;
        addEventListener("focusout", (event) => {
            input.readOnly = true;
        });
    });
}
inputBlock2();

function inputBlock3() {
    document.getElementById("image4").addEventListener("click", function (e) {
        var input = document.getElementById("phone_number");
        input.readOnly = false;
        addEventListener("focusout", (event) => {
            input.readOnly = true;
        });
    });
}
inputBlock3();
