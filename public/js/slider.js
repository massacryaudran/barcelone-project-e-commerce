let arm, gardener, search, bar, brouette, navbar;

function animation() {
    navbar = document.getElementsByClassName("navbar")[0];
    arm = document.getElementById("arm");
    gardener = document.getElementById("gardener");
    bar = document.getElementById("inputbar");
    brouette = document.getElementById("brouette");
    search = document.getElementById("search");
    arm.addEventListener("click", change_size);
    navbar.addEventListener("mouseleave", restore_size);
    navbar.addEventListener("mouseover", change_size);
    console.log("ready");
}
function change_size() {
    search.style.left = "-30px";
    search.style.top = "5px";
    bar.style.opacity = "1";
    bar.style.width = "250px";
    arm.style.transform = "rotate(-55deg) translateY(-8px)";
    brouette.style.transform = "rotate(88deg) translateY(-10px)";
    console.log("go");
}

function restore_size() {
    search.style.left = "";
    search.style.top = "";
    bar.style.opacity = "";
    bar.style.width = "";
    arm.style.transform = "";
    brouette.style.transform = "";
    console.log("back");
}
// animation();
window.onload = animation;

let slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
    showSlides((slideIndex += n));
}

function showSlides(n) {
    let i;
    let slides = document.getElementsByClassName("beginner-tips");
    if (n > slides.length) {
        slideIndex = 1;
    }
    if (n < 1) {
        slideIndex = slides.length;
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    slides[slideIndex - 1].style.display = "block";
}

let slideauto = 0;
autoSlides();

function autoSlides() {
    let i;
    let slides = document.getElementsByClassName("beginner-tips");
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    slideauto++;
    if (slideauto > slides.length) {
        slideauto = 1;
    }
    slides[slideauto - 1].style.display = "block";
    setTimeout(autoSlides, 5000);
}
