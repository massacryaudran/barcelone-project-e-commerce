"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pot2_js"],{

/***/ "./resources/js/Pot2.js":
/*!******************************!*\
  !*** ./resources/js/Pot2.js ***!
  \******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Model)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _react_three_drei__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @react-three/drei */ "./node_modules/@react-three/drei/core/useGLTF.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





function Model(props) {
  var _useGLTF = (0,_react_three_drei__WEBPACK_IMPORTED_MODULE_2__.useGLTF)("/storage/objects/pots/pot2/pot2.gltf"),
      nodes = _useGLTF.nodes,
      materials = _useGLTF.materials;

  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("group", _objectSpread(_objectSpread({}, props), {}, {
    dispose: null,
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
      geometry: nodes.Pot.geometry,
      material: materials.Concrete
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
      geometry: nodes.Ground.geometry,
      material: materials.Ground,
      position: [0, 0.42, -0.01],
      scale: 1.05
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
      geometry: nodes.Leaf.geometry,
      material: materials.Leaf,
      position: [0.25, 0.93, -0.04],
      rotation: [3.03, 0.41, -2.88],
      scale: 0.02
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
      geometry: nodes.Tree.geometry,
      material: materials.Tree,
      position: [0.23, 0.8, -0.01],
      rotation: [0.02, 0.1, -0.66],
      scale: 0.03
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
      geometry: nodes.Text.geometry,
      material: materials["Material.001"],
      position: [0.77, 0.19, 0.21],
      rotation: [Math.PI / 2, -0.18, -Math.PI / 2]
    })]
  }));
}
_react_three_drei__WEBPACK_IMPORTED_MODULE_2__.useGLTF.preload("/storage/objects/pots/pot2/pot2.gltf");

/***/ })

}]);