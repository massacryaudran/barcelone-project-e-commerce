function increaseValue() {
    var value = parseInt(document.getElementById("number").value, 10);
    if (value < 10) {
        value++;
        value = value < 10 ? value : value;
    }
    document.getElementById("number").value = value;
}

function decreaseValue() {
    const div1 = document.getElementById("number");
    var value = parseInt(div1.getAttribute("data-id").value, 10);
    if (value > 1) {
        value--;
        value = value < 10 ? value : value;
    }
    div1.getAttribute("data-id").value = value;
}
