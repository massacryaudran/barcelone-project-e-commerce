"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Plane_js"],{

/***/ "./resources/js/Plane.js":
/*!*******************************!*\
  !*** ./resources/js/Plane.js ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Model)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _react_three_drei__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @react-three/drei */ "./node_modules/@react-three/drei/core/useGLTF.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





function Model(props) {
  var _useGLTF = (0,_react_three_drei__WEBPACK_IMPORTED_MODULE_2__.useGLTF)("/storage/objects//box_maker/plane/plane.gltf"),
      nodes = _useGLTF.nodes,
      materials = _useGLTF.materials;

  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", _objectSpread(_objectSpread({}, props), {}, {
    dispose: null,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("mesh", {
      geometry: nodes.beech1006.geometry,
      material: materials.gradient_texture,
      position: [-0.86, 0.22, 3.02],
      rotation: [0, -1.07, 0],
      scale: 0.08,
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
        geometry: nodes.Cylinder.geometry,
        material: materials.Material,
        position: [-26.98, -1.51, -26.27],
        rotation: [0, 1.07, 0],
        scale: [97.34, 1.59, 97.99]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
        geometry: nodes.Plane026_1.geometry,
        material: materials.gradient_texture,
        position: [-22.74, -0.16, -24.15],
        rotation: [0, 1.07, 0],
        scale: 32.07
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("group", {
        position: [-95.1, -0.79, -23.67],
        rotation: [Math.PI / 2, 0, -1.51],
        scale: 0.72,
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
          geometry: nodes.tree004_1.geometry,
          material: materials["flower daffodil.001"]
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
          geometry: nodes.tree004_2.geometry,
          material: materials["leaf bush.001"]
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
          geometry: nodes.tree004_3.geometry,
          material: materials["plant01.001"]
        })]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("group", {
        position: [5.67, -0.79, 31.6],
        rotation: [Math.PI / 2, 0, -2.06],
        scale: 0.72,
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
          geometry: nodes.tree005_1.geometry,
          material: materials["flower daffodil.002"]
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
          geometry: nodes.tree005_2.geometry,
          material: materials["leaf bush.002"]
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
          geometry: nodes.tree005_3.geometry,
          material: materials["plant01.002"]
        })]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("group", {
        position: [-54.38, -0.79, -87.95],
        rotation: [Math.PI / 2, 0, -1.51],
        scale: 0.72,
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
          geometry: nodes.tree003_1.geometry,
          material: materials["flower daffodil.009"]
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
          geometry: nodes.tree003_2.geometry,
          material: materials["leaf bush.009"]
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
          geometry: nodes.tree003_3.geometry,
          material: materials["plant01.009"]
        })]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("group", {
        position: [39.13, -0.79, -29.41],
        rotation: [Math.PI / 2, 0, -1.72],
        scale: 0.72,
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
          geometry: nodes.tree006_1.geometry,
          material: materials["flower daffodil.003"]
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
          geometry: nodes.tree006_2.geometry,
          material: materials["leaf bush.003"]
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
          geometry: nodes.tree006_3.geometry,
          material: materials["plant01.003"]
        })]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("group", {
        position: [-64.64, -0.79, 29.16],
        rotation: [Math.PI / 2, 0, -1.51],
        scale: 0.72,
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
          geometry: nodes.tree007.geometry,
          material: materials["flower daffodil.004"]
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
          geometry: nodes.tree007_1.geometry,
          material: materials["leaf bush.004"]
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
          geometry: nodes.tree007_2.geometry,
          material: materials["plant01.004"]
        })]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("group", {
        position: [8.33, -0.79, -84.06],
        rotation: [Math.PI / 2, 0, -1.51],
        scale: 0.72,
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
          geometry: nodes.tree008.geometry,
          material: materials["flower daffodil.005"]
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
          geometry: nodes.tree008_1.geometry,
          material: materials["leaf bush.005"]
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
          geometry: nodes.tree008_2.geometry,
          material: materials["plant01.005"]
        })]
      })]
    })
  }));
}
_react_three_drei__WEBPACK_IMPORTED_MODULE_2__.useGLTF.preload("/storage/objects//box_maker/plane/plane.gltf");

/***/ })

}]);