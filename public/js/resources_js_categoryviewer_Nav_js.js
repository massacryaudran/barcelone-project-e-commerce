"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_categoryviewer_Nav_js"],{

/***/ "./resources/js/categoryviewer/Nav.js":
/*!********************************************!*\
  !*** ./resources/js/categoryviewer/Nav.js ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Navigation)
/* harmony export */ });
/* harmony import */ var _react_three_fiber__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @react-three/fiber */ "./node_modules/@react-three/fiber/dist/index-21f75f82.esm.js");

function Navigation(_a) {
  var _b;

  var cameraPosition = _a.cameraPosition;
  var cameraRotation = _a.cameraRotation;
  var camera = (0,_react_three_fiber__WEBPACK_IMPORTED_MODULE_0__.y)().camera;

  (_b = camera.position).set.apply(_b, cameraPosition);

  (_b = camera.rotation).set.apply(_b, cameraRotation);

  return null;
}

/***/ })

}]);