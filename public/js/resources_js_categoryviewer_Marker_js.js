"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_categoryviewer_marker_js"],{

/***/ "./resources/js/categoryviewer/marker.js":
/*!***********************************************!*\
  !*** ./resources/js/categoryviewer/marker.js ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Marker)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _react_three_drei__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @react-three/drei */ "./node_modules/@react-three/drei/web/Html.js");
/* harmony import */ var react_bootstrap_esm_Nav__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap/esm/Nav */ "./node_modules/react-bootstrap/esm/Nav.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");





function Marker(_a) {
  var position = _a.position,
      name = _a.name,
      id = _a.id,
      onMarkerClicked = _a.onMarkerClicked;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
    position: position,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(_react_three_drei__WEBPACK_IMPORTED_MODULE_2__.Html, {
      scaleFactor: 100,
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("div", {
        style: {
          display: "flex",
          flexDirection: "row"
        },
        onClick: function onClick() {
          return onMarkerClicked(id);
        },
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("div", {
          style: {
            border: "1px solid black",
            borderRadius: "10px",
            width: "1em",
            height: "fit-content",
            textAlign: "center",
            fontSize: "1em",
            fontFamily: "Antonio, sans-serif",
            color: "black",
            fontWeight: "bold"
          },
          children: id
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("div", {
          className: "box",
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(react_bootstrap_esm_Nav__WEBPACK_IMPORTED_MODULE_3__["default"].Link, {
            style: {
              textAlign: "center",
              fontSize: "1.5em",
              fontFamily: "Antonio, sans-serif",
              color: "black",
              textDecoration: "underline #9d5dea"
            },
            children: name
          })
        })]
      })
    })
  });
}

/***/ })

}]);