let slideSelected = 0;
let nbSlide = 0;
let arrowNext = null;
let arrowPrev = null;
const titles = ["Sustainable gardening", "Our boxes", "Our tutorial"];
function init() {
    nbSlide = document.getElementsByClassName("sectionInSlide").length;
    arrowNext = document.getElementById("arrowNext");
    arrowPrev = document.getElementById("arrowPrev");
    arrowNext.addEventListener("click", nextSlide);
    arrowPrev.addEventListener("click", previousSlide);
    nextSlide();
    previousSlide();
}

function nextSlide() {
    if (slideSelected + 1 >= nbSlide - 1) {
        arrowNext.classList.add("hidden");
    }
    arrowPrev.classList.remove("hidden");
    if (slideSelected + 1 >= nbSlide) {
        return;
    }
    document
        .getElementById("slider" + slideSelected)
        .classList.remove("stockRight");
    document
        .getElementById("slider" + slideSelected)
        .classList.add("stockLeft");
    document
        .getElementById("slidar" + slideSelected)
        .classList.remove("stockRight");
    document
        .getElementById("slidar" + slideSelected)
        .classList.add("stockLeft");
    slideSelected++;
    document
        .getElementById("slider" + slideSelected)
        .classList.remove("stockLeft");
    document
        .getElementById("slider" + slideSelected)
        .classList.remove("stockRight");
    document
        .getElementById("slidar" + slideSelected)
        .classList.remove("stockLeft");
    document
        .getElementById("slidar" + slideSelected)
        .classList.remove("stockRight");
    document.getElementById("title").innerHTML = titles[slideSelected];
}

function previousSlide() {
    if (slideSelected - 1 <= 0) {
        arrowPrev.classList.add("hidden");
    }
    arrowNext.classList.remove("hidden");
    if (slideSelected - 1 < 0) {
        return;
    }
    document
        .getElementById("slider" + slideSelected)
        .classList.add("stockRight");
    document
        .getElementById("slider" + slideSelected)
        .classList.remove("stockLeft");
    document
        .getElementById("slidar" + slideSelected)
        .classList.add("stockRight");
    document
        .getElementById("slidar" + slideSelected)
        .classList.remove("stockLeft");
    slideSelected--;
    document
        .getElementById("slider" + slideSelected)
        .classList.remove("stockLeft");
    document
        .getElementById("slider" + slideSelected)
        .classList.remove("stockRight");
    document
        .getElementById("slidar" + slideSelected)
        .classList.remove("stockLeft");
    document
        .getElementById("slidar" + slideSelected)
        .classList.remove("stockRight");
    document.getElementById("title").innerHTML = titles[slideSelected];
}

// window.addEventListener('load', init);
setTimeout((window.onload = init), 1000);
