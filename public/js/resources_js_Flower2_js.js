"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Flower2_js"],{

/***/ "./node_modules/@react-three/drei/core/useAnimations.js":
/*!**************************************************************!*\
  !*** ./node_modules/@react-three/drei/core/useAnimations.js ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "useAnimations": () => (/* binding */ useAnimations)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var three__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! three */ "./node_modules/three/build/three.module.js");
/* harmony import */ var _react_three_fiber__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @react-three/fiber */ "./node_modules/@react-three/fiber/dist/index-6f0019d1.esm.js");




function useAnimations(clips, root) {
  const ref = react__WEBPACK_IMPORTED_MODULE_0__.useRef();
  const [actualRef] = react__WEBPACK_IMPORTED_MODULE_0__.useState(() => root ? root instanceof three__WEBPACK_IMPORTED_MODULE_1__.Object3D ? {
    current: root
  } : root : ref); // eslint-disable-next-line prettier/prettier

  const [mixer] = react__WEBPACK_IMPORTED_MODULE_0__.useState(() => new three__WEBPACK_IMPORTED_MODULE_1__.AnimationMixer(undefined));
  const lazyActions = react__WEBPACK_IMPORTED_MODULE_0__.useRef({});
  const [api] = react__WEBPACK_IMPORTED_MODULE_0__.useState(() => {
    const actions = {};
    clips.forEach(clip => Object.defineProperty(actions, clip.name, {
      enumerable: true,

      get() {
        if (actualRef.current) {
          return lazyActions.current[clip.name] || (lazyActions.current[clip.name] = mixer.clipAction(clip, actualRef.current));
        }
      }

    }));
    return {
      ref: actualRef,
      clips,
      actions,
      names: clips.map(c => c.name),
      mixer
    };
  });
  (0,_react_three_fiber__WEBPACK_IMPORTED_MODULE_2__.A)((state, delta) => mixer.update(delta));
  react__WEBPACK_IMPORTED_MODULE_0__.useEffect(() => {
    const currentRoot = actualRef.current;
    return () => {
      // Clean up only when clips change, wipe out lazy actions and uncache clips
      lazyActions.current = {};
      Object.values(api.actions).forEach(action => {
        if (currentRoot) {
          mixer.uncacheAction(action, currentRoot);
        }
      });
    };
  }, [clips]);
  return api;
}




/***/ }),

/***/ "./resources/js/Flower2.js":
/*!*********************************!*\
  !*** ./resources/js/Flower2.js ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Model)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _react_three_drei__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @react-three/drei */ "./node_modules/@react-three/drei/core/useGLTF.js");
/* harmony import */ var _react_three_drei__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @react-three/drei */ "./node_modules/@react-three/drei/core/useAnimations.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
Auto-generated by: https://github.com/pmndrs/gltfjsx
*/




function Model(props) {
  var group = (0,react__WEBPACK_IMPORTED_MODULE_0__.useRef)();

  var _useGLTF = (0,_react_three_drei__WEBPACK_IMPORTED_MODULE_2__.useGLTF)("/storage/objects/box_maker/flowers2/flower2.gltf"),
      nodes = _useGLTF.nodes,
      materials = _useGLTF.materials,
      animations = _useGLTF.animations;

  var _useAnimations = (0,_react_three_drei__WEBPACK_IMPORTED_MODULE_3__.useAnimations)(animations, group),
      actions = _useAnimations.actions,
      names = _useAnimations.names;

  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(function () {
    // Reset and fade in animation after an index has been changed
    actions[names].reset().fadeIn(0.5).play(); // In the clean-up phase, fade it out

    return function () {
      return actions[names].fadeOut(0.5);
    };
  }, [actions, names]);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", _objectSpread(_objectSpread({
    ref: group
  }, props), {}, {
    dispose: null,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("group", {
      name: "Scene",
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("group", {
        name: "flower009_289",
        position: [29.17, 7.12, 6.39],
        rotation: [0, 0.21, 0],
        scale: 10.09,
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
          name: "Armature009_287",
          position: [0, -0.71, 0],
          rotation: [0, 1.57, 0],
          scale: 0.1
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
          name: "Plane018_288",
          position: [-0.07, -0.69, 0.08],
          rotation: [2.91, 0.38, -3.01],
          scale: 0.47
        })]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
        name: "Plane019_286"
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
        name: "Armature010_316",
        position: [0, -0.71, 0],
        rotation: [0, 1.57, 0],
        scale: 0.1
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
        name: "Plane021_315"
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
        name: "Plane020_317",
        position: [-0.07, -0.69, 0.08],
        rotation: [2.92, 0.24, -3.05],
        scale: 0.47,
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
          name: "mesh_21",
          geometry: nodes.mesh_21.geometry,
          material: materials.gradient_texture,
          morphTargetDictionary: nodes.mesh_21.morphTargetDictionary,
          morphTargetInfluences: nodes.mesh_21.morphTargetInfluences
        })
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("group", {
        name: "flower011_347",
        position: [0.73, 7.12, 0.46],
        rotation: [0, -0.04, 0],
        scale: 10.09,
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
          name: "Armature011_345",
          position: [0, -0.71, 0],
          rotation: [0, 1.57, 0],
          scale: 0.1,
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("group", {
            name: "GLTF_created_11",
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
              name: "Plane023_344"
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("primitive", {
              object: nodes.GLTF_created_11_rootJoint
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("skinnedMesh", {
              name: "Object_371",
              geometry: nodes.Object_371.geometry,
              material: materials.gradient_texture,
              skeleton: nodes.Object_371.skeleton
            })]
          })
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
          name: "Plane022_346",
          position: [-0.07, -0.69, 0.08],
          rotation: [2.92, 0.24, -3.05],
          scale: 0.47,
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("mesh", {
            name: "mesh_23",
            geometry: nodes.mesh_23.geometry,
            material: materials.gradient_texture,
            morphTargetDictionary: nodes.mesh_23.morphTargetDictionary,
            morphTargetInfluences: nodes.mesh_23.morphTargetInfluences
          })
        })]
      })]
    })
  }));
}
_react_three_drei__WEBPACK_IMPORTED_MODULE_2__.useGLTF.preload("/storage/objects/box_maker/flowers2/flower2.gltf");

/***/ })

}]);