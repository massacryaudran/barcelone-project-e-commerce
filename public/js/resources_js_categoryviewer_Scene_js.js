"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_categoryviewer_Scene_js"],{

/***/ "./node_modules/@react-three/drei/core/useAnimations.js":
/*!**************************************************************!*\
  !*** ./node_modules/@react-three/drei/core/useAnimations.js ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "useAnimations": () => (/* binding */ useAnimations)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var three__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! three */ "./node_modules/three/build/three.module.js");
/* harmony import */ var _react_three_fiber__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @react-three/fiber */ "./node_modules/@react-three/fiber/dist/index-6f0019d1.esm.js");




function useAnimations(clips, root) {
  const ref = react__WEBPACK_IMPORTED_MODULE_0__.useRef();
  const [actualRef] = react__WEBPACK_IMPORTED_MODULE_0__.useState(() => root ? root instanceof three__WEBPACK_IMPORTED_MODULE_1__.Object3D ? {
    current: root
  } : root : ref); // eslint-disable-next-line prettier/prettier

  const [mixer] = react__WEBPACK_IMPORTED_MODULE_0__.useState(() => new three__WEBPACK_IMPORTED_MODULE_1__.AnimationMixer(undefined));
  const lazyActions = react__WEBPACK_IMPORTED_MODULE_0__.useRef({});
  const [api] = react__WEBPACK_IMPORTED_MODULE_0__.useState(() => {
    const actions = {};
    clips.forEach(clip => Object.defineProperty(actions, clip.name, {
      enumerable: true,

      get() {
        if (actualRef.current) {
          return lazyActions.current[clip.name] || (lazyActions.current[clip.name] = mixer.clipAction(clip, actualRef.current));
        }
      }

    }));
    return {
      ref: actualRef,
      clips,
      actions,
      names: clips.map(c => c.name),
      mixer
    };
  });
  (0,_react_three_fiber__WEBPACK_IMPORTED_MODULE_2__.A)((state, delta) => mixer.update(delta));
  react__WEBPACK_IMPORTED_MODULE_0__.useEffect(() => {
    const currentRoot = actualRef.current;
    return () => {
      // Clean up only when clips change, wipe out lazy actions and uncache clips
      lazyActions.current = {};
      Object.values(api.actions).forEach(action => {
        if (currentRoot) {
          mixer.uncacheAction(action, currentRoot);
        }
      });
    };
  }, [clips]);
  return api;
}




/***/ }),

/***/ "./resources/js/categoryviewer/Scene.js":
/*!**********************************************!*\
  !*** ./resources/js/categoryviewer/Scene.js ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Model)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _react_three_drei__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @react-three/drei */ "./node_modules/@react-three/drei/core/useGLTF.js");
/* harmony import */ var _react_three_drei__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @react-three/drei */ "./node_modules/@react-three/drei/core/useAnimations.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





function Model(props) {
  var group = (0,react__WEBPACK_IMPORTED_MODULE_0__.useRef)();

  var _useGLTF = (0,_react_three_drei__WEBPACK_IMPORTED_MODULE_2__.useGLTF)("/storage/objects/category/animation_leaves/scene.gltf"),
      nodes = _useGLTF.nodes,
      materials = _useGLTF.materials,
      animations = _useGLTF.animations;

  var _useAnimations = (0,_react_three_drei__WEBPACK_IMPORTED_MODULE_3__.useAnimations)(animations, group),
      actions = _useAnimations.actions,
      names = _useAnimations.names;

  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(function () {
    // Reset and fade in animation after an index has been changed
    actions[names].reset().fadeIn(0.5).play(); // In the clean-up phase, fade it out

    return function () {
      return actions[names].fadeOut(0.5);
    };
  }, [actions, names]);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", _objectSpread(_objectSpread({
    ref: group
  }, props), {}, {
    dispose: null,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
      name: "Sketchfab_Scene",
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
        name: "Sketchfab_model",
        rotation: [-Math.PI / 2, 0, 0],
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
          name: "root",
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
            name: "GLTF_SceneRootNode",
            rotation: [Math.PI / 2, 0, 0],
            children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
              name: "RootNode0_0",
              scale: 2,
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                name: "skeletal22_22",
                children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("group", {
                  name: "GLTF_created_0",
                  children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("primitive", {
                    object: nodes.GLTF_created_0_rootJoint
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                    name: "falling_leaves_12_2_correction",
                    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                      name: "falling_leaves_12_2"
                    })
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                    name: "falling_leaves_23_3_correction",
                    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                      name: "falling_leaves_23_3"
                    })
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                    name: "falling_leaves_34_4_correction",
                    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                      name: "falling_leaves_34_4"
                    })
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                    name: "falling_leaves_45_5_correction",
                    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                      name: "falling_leaves_45_5"
                    })
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                    name: "falling_leaves_56_6_correction",
                    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                      name: "falling_leaves_56_6"
                    })
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                    name: "falling_leaves_67_7_correction",
                    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                      name: "falling_leaves_67_7"
                    })
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                    name: "falling_leaves_78_8_correction",
                    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                      name: "falling_leaves_78_8"
                    })
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                    name: "falling_leaves_89_9_correction",
                    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                      name: "falling_leaves_89_9"
                    })
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                    name: "falling_leaves_910_10_correction",
                    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                      name: "falling_leaves_910_10"
                    })
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                    name: "falling_leaves_1011_11_correction",
                    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                      name: "falling_leaves_1011_11"
                    })
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                    name: "falling_leaves_1112_12_correction",
                    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                      name: "falling_leaves_1112_12"
                    })
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                    name: "falling_leaves_1213_13_correction",
                    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                      name: "falling_leaves_1213_13"
                    })
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                    name: "falling_leaves_1314_14_correction",
                    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                      name: "falling_leaves_1314_14"
                    })
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                    name: "falling_leaves_1415_15_correction",
                    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                      name: "falling_leaves_1415_15"
                    })
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                    name: "falling_leaves_1516_16_correction",
                    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                      name: "falling_leaves_1516_16"
                    })
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                    name: "falling_leaves_1617_17_correction",
                    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                      name: "falling_leaves_1617_17"
                    })
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                    name: "falling_leaves_1718_18_correction",
                    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                      name: "falling_leaves_1718_18"
                    })
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                    name: "falling_leaves_1819_19_correction",
                    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                      name: "falling_leaves_1819_19"
                    })
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                    name: "falling_leaves_1920_20_correction",
                    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                      name: "falling_leaves_1920_20"
                    })
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                    name: "falling_leaves_2021_21_correction",
                    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("group", {
                      name: "falling_leaves_2021_21"
                    })
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("skinnedMesh", {
                    name: "Object_30",
                    geometry: nodes.Object_30.geometry,
                    material: materials.material_0,
                    skeleton: nodes.Object_30.skeleton
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("skinnedMesh", {
                    name: "Object_33",
                    geometry: nodes.Object_33.geometry,
                    material: materials.material_1,
                    skeleton: nodes.Object_33.skeleton
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("skinnedMesh", {
                    name: "Object_36",
                    geometry: nodes.Object_36.geometry,
                    material: materials.material_2,
                    skeleton: nodes.Object_36.skeleton
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("skinnedMesh", {
                    name: "Object_39",
                    geometry: nodes.Object_39.geometry,
                    material: materials.material_3,
                    skeleton: nodes.Object_39.skeleton
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("skinnedMesh", {
                    name: "Object_42",
                    geometry: nodes.Object_42.geometry,
                    material: materials.material_4,
                    skeleton: nodes.Object_42.skeleton
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("skinnedMesh", {
                    name: "Object_45",
                    geometry: nodes.Object_45.geometry,
                    material: materials.material_5,
                    skeleton: nodes.Object_45.skeleton
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("skinnedMesh", {
                    name: "Object_48",
                    geometry: nodes.Object_48.geometry,
                    material: materials.material_6,
                    skeleton: nodes.Object_48.skeleton
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("skinnedMesh", {
                    name: "Object_51",
                    geometry: nodes.Object_51.geometry,
                    material: materials.material_7,
                    skeleton: nodes.Object_51.skeleton
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("skinnedMesh", {
                    name: "Object_54",
                    geometry: nodes.Object_54.geometry,
                    material: materials.material_8,
                    skeleton: nodes.Object_54.skeleton
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("skinnedMesh", {
                    name: "Object_57",
                    geometry: nodes.Object_57.geometry,
                    material: materials.material_9,
                    skeleton: nodes.Object_57.skeleton
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("skinnedMesh", {
                    name: "Object_60",
                    geometry: nodes.Object_60.geometry,
                    material: materials.material_10,
                    skeleton: nodes.Object_60.skeleton
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("skinnedMesh", {
                    name: "Object_63",
                    geometry: nodes.Object_63.geometry,
                    material: materials.material_11,
                    skeleton: nodes.Object_63.skeleton
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("skinnedMesh", {
                    name: "Object_66",
                    geometry: nodes.Object_66.geometry,
                    material: materials.material_12,
                    skeleton: nodes.Object_66.skeleton
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("skinnedMesh", {
                    name: "Object_69",
                    geometry: nodes.Object_69.geometry,
                    material: materials.material_13,
                    skeleton: nodes.Object_69.skeleton
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("skinnedMesh", {
                    name: "Object_72",
                    geometry: nodes.Object_72.geometry,
                    material: materials.material_14,
                    skeleton: nodes.Object_72.skeleton
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("skinnedMesh", {
                    name: "Object_75",
                    geometry: nodes.Object_75.geometry,
                    material: materials.material_15,
                    skeleton: nodes.Object_75.skeleton
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("skinnedMesh", {
                    name: "Object_78",
                    geometry: nodes.Object_78.geometry,
                    material: materials.material_16,
                    skeleton: nodes.Object_78.skeleton
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("skinnedMesh", {
                    name: "Object_81",
                    geometry: nodes.Object_81.geometry,
                    material: materials.material_17,
                    skeleton: nodes.Object_81.skeleton
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("skinnedMesh", {
                    name: "Object_84",
                    geometry: nodes.Object_84.geometry,
                    material: materials.material_18,
                    skeleton: nodes.Object_84.skeleton
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("skinnedMesh", {
                    name: "Object_87",
                    geometry: nodes.Object_87.geometry,
                    material: materials.material_19,
                    skeleton: nodes.Object_87.skeleton
                  })]
                })
              })
            })
          })
        })
      })
    })
  }));
}
_react_three_drei__WEBPACK_IMPORTED_MODULE_2__.useGLTF.preload("/storage/objects/category/animation_leaves/scene.gltf");

/***/ })

}]);